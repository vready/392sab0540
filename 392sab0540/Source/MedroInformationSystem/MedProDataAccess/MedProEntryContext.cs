﻿using System.Data.Entity;
using System.Net;
using MedProEntryEngine;

namespace MedProDataAccess
{
    /// <summary>
    /// The med pro entry context class.
    /// </summary>
    public class MedProEntryContext : DbContext 
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MedProEntryContext"/> class.
        /// </summary>
        public MedProEntryContext()
            :base("MedProEntryContext")
        {
        }

        /// <summary>
        /// Gets or sets the database table for the patients.
        /// </summary>
        public DbSet<Patient> Patients { get; set; }

        /// <summary>
        /// Gets or sets the database table for the notes.
        /// </summary>
        public DbSet<Note> Notes { get; set; }

        /// <summary>
        /// Gets or sets the database table for the insurances.
        /// </summary>
        public DbSet<Insurance> Insurances { get; set; }

        /// <summary>
        /// Gets or sets the database lines.
        /// </summary>
        public DbSet<NoteLine> Lines { get; set; }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<EmployeeLoginInfo> EmployeeLoginInfos { get; set; }
        
        public DbSet<Address> Addresses { get; set; }
    }
}
