namespace MedProDataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddPatientBillingCode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Insurances", "PatientBillingCode", c => c.String());
            Sql("Update dbo.Insurances SET PatientBillingCode = UPPER(CompanyName + CONVERT(varchar, InsuranceType) + CONVERT(varchar, PatientId)) WHERE dbo.Patient.Id = dbo.Insurances.PatientId");
        }
        
        public override void Down()
        {
            DropColumn("dbo.Insurances", "PatientBillingCode");
        }
    }
}
