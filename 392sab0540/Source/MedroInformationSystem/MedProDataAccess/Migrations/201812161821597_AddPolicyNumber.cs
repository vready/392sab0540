namespace MedProDataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class AddPolicyNumber : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Insurances", "PolicyNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Insurances", "PolicyNumber");
        }
    }
}
