namespace MedProDataAccess.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AddressLine1 = c.String(nullable: false),
                        AddressLine2 = c.String(),
                        City = c.String(nullable: false),
                        State = c.Int(nullable: false),
                        ZipCode = c.String(nullable: false),
                        IsArchived = c.Boolean(nullable: false),
                        StartDate = c.DateTime(),
                        EndDate = c.DateTime(),
                        PatientId = c.Int(),
                        InsuranceId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Insurances", t => t.InsuranceId)
                .ForeignKey("dbo.Patients", t => t.PatientId)
                .Index(t => t.PatientId)
                .Index(t => t.InsuranceId);
            
            CreateTable(
                "dbo.Insurances",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PatientId = c.Int(nullable: false),
                        CompanyName = c.String(nullable: false, maxLength: 150),
                        InsuranceType = c.Int(nullable: false),
                        StartDate = c.DateTime(nullable: false),
                        EndDate = c.DateTime(),
                        IsArchived = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patients", t => t.PatientId, cascadeDelete: true)
                .Index(t => t.PatientId);
            
            CreateTable(
                "dbo.Patients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        MiddleName = c.String(maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        DateOfBirth = c.DateTime(),
                        Phone = c.String(),
                        Email = c.String(maxLength: 255),
                        Gender = c.Int(nullable: false),
                        MedicalRecordNumber = c.Int(nullable: false),
                        IsMinor = c.Boolean(nullable: false),
                        IsArchived = c.Boolean(nullable: false),
                        IsNotNewPatient = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.NoteLines",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PatientNote = c.String(nullable: false),
                        Timestamp = c.DateTime(nullable: false),
                        NoteId = c.Int(nullable: false),
                        IsArchived = c.Boolean(nullable: false),
                        Patient_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Notes", t => t.NoteId, cascadeDelete: true)
                .ForeignKey("dbo.Patients", t => t.Patient_Id)
                .Index(t => t.NoteId)
                .Index(t => t.Patient_Id);
            
            CreateTable(
                "dbo.Notes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PatientId = c.Int(nullable: false),
                        VisitDate = c.DateTime(),
                        IsArchived = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Patients", t => t.PatientId, cascadeDelete: true)
                .Index(t => t.PatientId);
            
            CreateTable(
                "dbo.EmployeeLoginInfoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EmployeeId = c.Int(nullable: false),
                        ComputerIpAddress = c.String(),
                        TimeStamp = c.DateTime(),
                        IsArchived = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Employees", t => t.EmployeeId, cascadeDelete: true)
                .Index(t => t.EmployeeId);
            
            CreateTable(
                "dbo.Employees",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        IsArchived = c.Boolean(nullable: false),
                        FirstName = c.String(),
                        LastName = c.String(),
                        EmployeeNumber = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.EmployeeLoginInfoes", "EmployeeId", "dbo.Employees");
            DropForeignKey("dbo.NoteLines", "Patient_Id", "dbo.Patients");
            DropForeignKey("dbo.Notes", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.NoteLines", "NoteId", "dbo.Notes");
            DropForeignKey("dbo.Insurances", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.Addresses", "PatientId", "dbo.Patients");
            DropForeignKey("dbo.Addresses", "InsuranceId", "dbo.Insurances");
            DropIndex("dbo.EmployeeLoginInfoes", new[] { "EmployeeId" });
            DropIndex("dbo.Notes", new[] { "PatientId" });
            DropIndex("dbo.NoteLines", new[] { "Patient_Id" });
            DropIndex("dbo.NoteLines", new[] { "NoteId" });
            DropIndex("dbo.Insurances", new[] { "PatientId" });
            DropIndex("dbo.Addresses", new[] { "InsuranceId" });
            DropIndex("dbo.Addresses", new[] { "PatientId" });
            DropTable("dbo.Employees");
            DropTable("dbo.EmployeeLoginInfoes");
            DropTable("dbo.Notes");
            DropTable("dbo.NoteLines");
            DropTable("dbo.Patients");
            DropTable("dbo.Insurances");
            DropTable("dbo.Addresses");
        }
    }
}
