﻿using System;
using System.Collections.Generic;
using System.Linq;
using MedProEntryEngine;

namespace MedProDataAccess
{
    /// <summary>
    /// The repository class.
    /// </summary>
    public class Repository
    {
        /// <summary>
        /// Initializes a new database context.
        /// </summary>
        private MedProEntryContext context = new MedProEntryContext();

        /// <summary>
        /// The add patient method.
        /// </summary>
        /// <param name="patient">The patient being passed in.</param>
        public void AddPatient(Patient patient)
        {
            if(this.ContainsPatient(patient) != true)
            {
                this.context.Patients.Add(patient);
                if (this.PatientAdded != null)
                {
                    this.PatientAdded(this, new PatientEventArgs(patient));
                }
            }
        }

        /// <summary>
        /// The contains patient method.
        /// </summary>
        /// <param name="patient"></param>
        /// <returns>The patient being requested in the patients list.</returns>
        private bool ContainsPatient(Patient patient)
        {
             return this.GetPatient(patient.Id) != null;
        }

        /// <summary>
        /// The get patients method.
        /// </summary>
        /// <returns>A list of patients.</returns>
        public List<Patient> GetPatients()
        {
            return this.context.Patients.Where(p => !p.IsArchived).ToList();
        }

        public void RemovePatient(Patient patient)
        {
            patient.IsArchived = true;

            if(this.PatientRemoved != null)
            {
                this.PatientRemoved(this, new PatientEventArgs(patient));
            }
        }

        /// <summary>
        /// The add insurance method.
        /// </summary>
        /// <param name="insurance">The insurance being passed in.</param>
        public void AddInsurance(Insurance insurance)
        {
            if (this.ContainsInsurance(insurance) != true)
            {
                this.context.Insurances.Add(insurance);
                if (this.InsuranceAdded != null)
                {
                    this.InsuranceAdded(this, new InsuranceEventArgs(insurance));
                }
            }

        }

        /// <summary>
        /// The contains insurance method.
        /// </summary>
        /// <param name="insurance">The insurance being passed in.</param>
        /// <returns>The insurance information being requested.</returns>
        private bool ContainsInsurance(Insurance insurance)
        {
            return this.GetInsurance(insurance.Id) != null;
        }

        /// <summary>
        /// The get insurance method.
        /// </summary>
        /// <returns>The list of insurances.</returns>
        public List<Insurance> GetInsurance()
        {
            return this.context.Insurances.Where(p => !p.IsArchived).ToList();
        }

        public void RemoveInsurance(Insurance insurance)
        {
            insurance.IsArchived = true;

            if (this.InsuranceRemoved != null)
            {
                this.InsuranceRemoved(this, new InsuranceEventArgs(insurance));
            }
        }

        /// <summary>
        /// The add note method.
        /// </summary>
        /// <param name="note">The note being passed in.</param>
        public void AddNote(Note note)
        {
            if (this.ContainsNote(note) != true)
            {
                this.context.Notes.Add(note);
                if (this.NoteAdded != null)
                {
                    this.NoteAdded(this, new NoteEventArgs(note));
                }
            }
        }

        /// <summary>
        /// The contains note method.
        /// </summary>
        /// <param name="note">The note being passed in.</param>
        /// <returns>The note being requested.</returns>
        private bool ContainsNote(Note note)
        {
            return this.GetNote(note.Id) != null;
        }

        /// <summary>
        /// The get notes method.
        /// </summary>
        /// <returns>The list of patient notes.</returns>
        public List<Note> GetNotes()
        {
            return this.context.Notes.Where(p => !p.IsArchived).ToList();
        }

        public void RemoveNote(Note note)
        {
            note.IsArchived = true;

            if (this.NoteRemoved != null)
            {
                this.NoteRemoved(this, new NoteEventArgs(note));
            }
        }

        /// <summary>
        /// The save to database method.
        /// </summary>
        public void SaveToDatabase()
        {
            try
            {
                this.context.SaveChanges();
            }
            catch
            {

            }
            
        }

        /// <summary>
        /// The get note method.
        /// </summary>
        /// <param name="id">The id being passed in.</param>
        /// <returns>The note being requested.</returns>
        private Note GetNote(int id)
        {
            return this.context.Notes.Find(id);
        }

        /// <summary>
        /// The get patient method.
        /// </summary>
        /// <param name="id">The patient id being passed in.</param>
        /// <returns>The patient being requested.</returns>
        private Patient GetPatient(int id)
        {
            return this.context.Patients.Find(id);
        }


        /// <summary>
        /// The get insurance method.
        /// </summary>
        /// <param name="id">The id being passed in.</param>
        /// <returns>The insurance being requested.</returns>
        private Insurance GetInsurance(int id)
        {
            return this.context.Insurances.Find(id);
        }

        /// <summary>
        /// The add line method.
        /// </summary>
        /// <param name="line">The note line being added.</param>
        public void AddLine(NoteLine line)
        {
            if (this.ContainsLine(line) != true)
            {
                this.context.Lines.Add(line);
                if (this.LineAdded != null)
                {
                    this.LineAdded(this, new NoteLineEventArgs(line));
                }
            }
        }

        /// <summary>
        /// The containes line method.
        /// </summary>
        /// <param name="line">The line being passed in.</param>
        /// <returns>A true or false value.</returns>
        private bool ContainsLine(NoteLine line)
        {
            return this.GetLine(line.Id) != null;
        }

        /// <summary>
        /// The get line metho.d
        /// </summary>
        /// <param name="id">The id being passed in.</param>
        /// <returns>Returns a note line.</returns>
        private NoteLine GetLine(int id)
        {
            return this.context.Lines.Find(id);
        }

        /// <summary>
        /// The get patients method.
        /// </summary>
        /// <returns>A list of patients.</returns>
        public List<NoteLine> GetLines()
        {
            return this.context.Lines.Where(p => !p.IsArchived).ToList();
        }

        public void RemoveLine(NoteLine line)
        {
            line.IsArchived = true;

            if (this.LineRemoved != null)
            {
                this.LineRemoved(this, new NoteLineEventArgs(line));
            }
        }

        /// <summary>
        /// The get employee method.
        /// </summary>
        /// <returns>A list of employees.</returns>
        public Employee GetEmployee(string employeeNumber, string password)
        {
            return this.context.Employees.SingleOrDefault(e => e.EmployeeNumber == employeeNumber && e.Password == password);
        }        

        public void AddAddress(Address address)
        {
            if (this.ContainsAddress(address) != true)
            {
                this.context.Addresses.Add(address);
                if (this.LineAdded != null)
                {
                    this.AddressAdded(this, new AddressEventArgs(address));
                }
            }
        }

        public List<Employee> GetEmployee()
        {
            return this.context.Employees.ToList();
        }

        /// <summary>
        /// The contains address method.
        /// </summary>
        /// <param name="line">The line being passed in.</param>
        /// <returns>A true or false value.</returns>
        private bool ContainsAddress(Address address)
        {
            return this.GetAddress(address.Id) != null;
        }

        /// <summary>
        /// The get line metho.d
        /// </summary>
        /// <param name="id">The id being passed in.</param>
        /// <returns>Returns an address.</returns>
        private Address GetAddress(int id)
        {
            return this.context.Addresses.Find(id);
        }

        /// <summary>
        /// The get address method.
        /// </summary>
        /// <returns>A list of addresses..</returns>
        public List<Address> GetAddresses()
        {
            return this.context.Addresses.Where(p => !p.IsArchived).ToList();
        }

        /// <summary>
        /// The remove address method.
        /// </summary>
        /// <param name="address">The address being passed in.</param>
        public void RemoveAddress(Address address)
        {
            address.IsArchived = true;

            if (this.AddressRemoved != null)
            {
                this.AddressRemoved(this, new AddressEventArgs(address));
            }
        }

        /// <summary>
        /// The add employee login info method.
        /// </summary>
        /// <param name="employeeLoginInfo">The employee login being passed in.</param>
        public void AddEmployeeLoginInfo(EmployeeLoginInfo employeeLoginInfo)
        {
            if (this.ContainsEmployeeLogin(employeeLoginInfo) != true)
            {
                this.context.EmployeeLoginInfos.Add(employeeLoginInfo);
                if (this.NoteAdded != null)
                {
                    this.EmployeeLoginInfoAdded(this, new EmployeeLoginInfoEventArgs(employeeLoginInfo));
                }
            }
        }

        /// <summary>
        /// The contains employee login method.
        /// </summary>
        /// <param name="employeeLoginInfo">The employee login being passed in.</param>
        /// <returns></returns>
        private bool ContainsEmployeeLogin(EmployeeLoginInfo employeeLoginInfo)
        {
            return this.GetEmployeeLoginInfo(employeeLoginInfo.Id) != null;
        }

        /// <summary>
        /// The get employee login info method.
        /// </summary>
        /// <param name="id">The id being passed in.</param>
        /// <returns>REturns and employee.</returns>
        private EmployeeLoginInfo GetEmployeeLoginInfo(int id)
        {
            return this.context.EmployeeLoginInfos.Find(id);
        }

        public List<EmployeeLoginInfo> GetEmployeeInfos()
        {
            return this.context.EmployeeLoginInfos.Where(p => !p.IsArchived).ToList();
        }


        /// <summary>
        /// The add insurance method.
        /// </summary>
        /// <param name="insurance">The insurance being passed in.</param>

        /// <summary>
        /// The patient added event handler.
        /// </summary>
        public event EventHandler<PatientEventArgs> PatientAdded;

        /// <summary>
        /// The Insurance added event handler.
        /// </summary>
        public event EventHandler<InsuranceEventArgs> InsuranceAdded;

        /// <summary>
        /// The note added event handler.
        /// </summary>
        public event EventHandler<NoteEventArgs> NoteAdded;

        /// <summary>
        /// The line added event.
        /// </summary>
        public event EventHandler<NoteLineEventArgs> LineAdded;

        public event EventHandler<AddressEventArgs> AddressAdded;

        public event EventHandler<PatientEventArgs> PatientRemoved;

        public event EventHandler<InsuranceEventArgs> InsuranceRemoved;

        public event EventHandler<NoteEventArgs> NoteRemoved;

        public event EventHandler<NoteLineEventArgs> LineRemoved;

        public event EventHandler<AddressEventArgs> AddressRemoved;

        public event EventHandler<EmployeeLoginInfoEventArgs> EmployeeLoginInfoAdded;
    }
}
