﻿namespace MedProEntryEngine
{
    public enum InsuranceType
    {
        HMO,
        PPO,
        POS,
        EPO,
        HDHP,
        Supplemental,
        SelfPay
    }
}
