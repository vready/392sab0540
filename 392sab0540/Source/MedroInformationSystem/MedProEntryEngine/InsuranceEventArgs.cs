﻿namespace MedProEntryEngine
{
    /// <summary>
    /// The insurance event argument class.
    /// </summary>
    public class InsuranceEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InsuranceEventArgs"/> class.
        /// </summary>
        /// <param name="insurance">The insurance being passed in.</param>
        public InsuranceEventArgs(Insurance insurance)
        {
            this.Insurance = insurance;
        }

        /// <summary>
        /// Gets or sets the insurance field.
        /// </summary>
        public Insurance Insurance { get; set; }
    }
}
