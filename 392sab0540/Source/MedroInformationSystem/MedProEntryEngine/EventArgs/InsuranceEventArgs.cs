﻿namespace MedProEntryEngine
{
    public class InsuranceEventArgs
    {
        public InsuranceEventArgs(Insurance insurance)
        {
            this.Insurance = insurance;
        }

        public Insurance Insurance { get; set; }
    }
}
