﻿namespace MedProEntryEngine
{
    public class PatientEventArgs
    {
        public PatientEventArgs(Patient patient)
        {
            this.Patient = patient;
        }

        public Patient Patient { get; set; }
    }
}
