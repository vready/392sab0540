﻿namespace MedProEntryEngine
{
    public class NoteEventArgs
    {
        public NoteEventArgs(Note note)
        {
            this.Note = note;
        }

        public Note Note { get; set; }
    }
}
