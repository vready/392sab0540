﻿namespace MedProEntryEngine
{
    /// <summary>
    /// The address event arguments class.
    /// </summary>
    public class AddressEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AddressEventArgs"/> class.
        /// </summary>
        /// <param name="address">The address being passed in.</param>
        public AddressEventArgs(Address address)
        {
            this.Address = address;
        }

        /// <summary>
        /// Gets or sets the insurance field.
        /// </summary>
        public Address Address { get; set; }
    }
}
