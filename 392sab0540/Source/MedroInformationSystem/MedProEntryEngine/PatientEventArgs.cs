﻿namespace MedProEntryEngine
{
    /// <summary>
    /// The patient event args class.
    /// </summary>
    public class PatientEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PatientEventArgs"/> class.
        /// </summary>
        /// <param name="patient">The patient being passed in.</param>
        public PatientEventArgs(Patient patient)
        {
            this.Patient = patient;
        }

        /// <summary>
        /// Gets or sets the value of patient.
        /// </summary>
        public Patient Patient { get; set; }
    }
}
