﻿namespace MedProEntryEngine
{
    /// <summary>
    /// The note event arguments class.
    /// </summary>
    public class NoteEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NoteEventArgs"/> class.
        /// </summary>
        /// <param name="note">The note being passed in.</param>
        public NoteEventArgs(Note note)
        {
            this.Note = note;
        }

        /// <summary>
        /// Gets or sets the note field.
        /// </summary>
        public Note Note { get; set; }
    }
}
