﻿namespace MedProEntryEngine
{
    /// <summary>
    /// The note line event arguments.
    /// </summary>
    public class NoteLineEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NoteLineEventArgs"/> class.
        /// </summary>
        /// <param name="line">The line being passed in.</param>
        public NoteLineEventArgs(NoteLine line)
        {
            this.Line = line;
        }

        /// <summary>
        /// Gets or sets the insurance field.
        /// </summary>
        public NoteLine Line { get; set; }
    }
}
