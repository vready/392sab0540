﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MedProEntryEngine
{
    /// <summary>
    /// The Addres class.
    /// </summary>
    public class Address
    {
        /// <summary>
        /// Gets or sets the address id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the Address line 1.
        /// </summary>
        [Required]
        public string AddressLine1 { get; set; }

        /// <summary>
        /// Gets or sets the address line 2.
        /// </summary>
        public string AddressLine2 { get; set; }

        /// <summary>
        /// Gets or sets the address city.
        /// </summary>
        [Required]
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the address state.
        /// </summary>
        [Required]
        public State State { get; set; }

        /// <summary>
        /// Gets or sets the address zip code.
        /// </summary>
        [Required]
        public string ZipCode { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether address is archived or not.
        /// </summary>
        public bool IsArchived { get; set; }

        /// <summary>
        /// Gets or sets the address start date.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets the address patient id.
        /// </summary>
        public int? PatientId { get; set; }

        /// <summary>
        /// Gets or sets the address patient.
        /// </summary>
        public virtual Patient Patient { get; set; }

        /// <summary>
        /// Gets or sets the address insurance id.
        /// </summary>
        public int? InsuranceId { get; set; }

        /// <summary>
        /// Gets or sets the address insurance.
        /// </summary>
        public virtual Insurance Insurance { get; set; }

        /// <summary>
        /// The over ride string method for address.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.AddressLine1 + " " + this.AddressLine2 + ", " + this.City + ", " + this.State + " " + this.ZipCode;
        }
    }
}
