﻿namespace MedProEntryEngine
{
    /// <summary>
    /// The employee login info event args class.
    /// </summary>
    public class EmployeeLoginInfoEventArgs
    {
        /// <summary>
        /// Initializes an instances of the <see cref="EmployeeLoginInfoEventArgs"/> class.
        /// </summary>
        /// <param name="employeeLoginInfo">The employee login info being passed in.</param>
        public EmployeeLoginInfoEventArgs(EmployeeLoginInfo employeeLoginInfo)
        {
            this.EmployeeLoginInfo = employeeLoginInfo;
        }

        /// <summary>
        /// Gets or sets the employee login ingo field.
        /// </summary>
        public EmployeeLoginInfo EmployeeLoginInfo { get; set; }
    }
}
