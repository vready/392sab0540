﻿using System.Collections.Generic;

namespace MedProEntryEngine
{
    /// <summary>
    /// The employee class.
    /// </summary>
    public class Employee
    {
        public Employee()
        {
            this.EmployeeLogins = new List<EmployeeLoginInfo>();
        }

        /// <summary>
        /// The employee id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is archived is true or false.
        /// </summary>
        public bool IsArchived { get; set; }

        /// <summary>
        /// The employee first name field.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// The employee last name field.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the value of the list of orders.
        /// </summary>
        public virtual ICollection<EmployeeLoginInfo> EmployeeLogins { get; set; }

        /// <summary>
        /// Gets or sets the employee number.
        /// </summary>
        public string EmployeeNumber { get; set; }

        /// <summary>
        /// Gets or sets the employee password.
        /// </summary>
        public string Password { get; set; }        
    }
}
