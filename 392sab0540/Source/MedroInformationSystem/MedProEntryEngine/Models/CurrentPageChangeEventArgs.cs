﻿namespace MedProEntryEngine
{
    /// <summary>
    /// The current page change event argument class.
    /// </summary>
    public class CurrentPageChangeEventArgs
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CurrentPageChangeEventArgs"/> class.
        /// </summary>
        /// <param name="startIndex">The start index.</param>
        /// <param name="itemCount">The item count.</param>
        public CurrentPageChangeEventArgs(int startIndex, int itemCount)
        {
        }

        /// <summary>
        /// Gets the start index.
        /// </summary>
        public int StartIndex { get; private set; }

        /// <summary>
        /// Gets the item count.
        /// </summary>
        public int ItemCount { get; private set; }
    }
}
