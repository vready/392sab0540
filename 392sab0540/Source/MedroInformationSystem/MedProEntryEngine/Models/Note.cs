﻿using System;
using System.Collections.Generic;

namespace MedProEntryEngine
{
    /// <summary>
    /// The note class.
    /// </summary>
    public class Note
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Note"/> class.
        /// </summary>
        public Note()
        {
            this.Lines = new List<NoteLine>();
        }

        /// <summary>
        /// The note id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the note line collection.
        /// </summary>
        public virtual ICollection<NoteLine> Lines { get; set; }

        /// <summary>
        /// Gets or sets the patient id.
        /// </summary>
        public int PatientId { get; set; }

        /// <summary>
        /// Gets or sets the patient associated with the note.
        /// </summary>
        public virtual Patient Patient { get; set; }

        /// <summary>
        /// Gets or sets the visit date.
        /// </summary>
        public DateTime? VisitDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is archived is true or not.
        /// </summary>
        public bool IsArchived { get; set; }
    }
}
