﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace MedProEntryEngine
{
    /// <summary>
    /// The patient class.
    /// </summary>
    public class Patient : IDataErrorInfo
    {
        /// <summary>
        /// The properties to validate array.
        /// </summary>
        private static readonly string[] PropertiesToValidate = new string[] { "FirstName", "LastName", "Phone", "Email" };

        /// <summary>
        /// Initializes a new instance of the <see cref="Patient"/> class.
        /// </summary>
        public Patient()
        {
            this.Notes = new List<Note>();
            this.Insurances = new List<Insurance>();
            this.Addresses = new List<Address>();
        }

        /// <summary>
        /// Gets the error message.
        /// </summary>
        public string Error
        {
            get
            {
                return null;
            }
        }
        
        /// <summary>
        /// The patient id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets a value indicating whether there are no errors for all properties being checked.
        /// </summary>
        public bool IsValid
        {
            get
            {
                foreach (string s in PropertiesToValidate)
                {
                    if (this.GetValidationError(s) != null)
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// Gets or sets the patient's first name.
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the patient's middle name.
        /// </summary>
        [MaxLength(100)]
        public string MiddleName { get; set; }

        /// <summary>
        /// Gets or sets the patient's last name.
        /// </summary>
        [Required]
        [MaxLength(100)]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the patient's date of birth.
        /// </summary>
        public DateTime? DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the patient's phone.
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Gets or sets the patient's email.
        /// </summary>
        [MaxLength(255)]
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the patient's first name.
        /// </summary>
        [Required]
        public Gender Gender { get; set; }

        /// <summary>
        /// Gets or sets the patient's medical record number.
        /// </summary>
        public int MedicalRecordNumber { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the patient's is minor.
        /// </summary>
        public bool IsMinor { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the patien has been archived.
        /// </summary>
        public bool IsArchived { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether a patient is new or not.
        /// </summary>
        public bool IsNotNewPatient { get; set; }

        /// <summary>
        /// Gets or sets the list of insurances.
        /// </summary>
        public virtual ICollection<Insurance> Insurances { get; set; }

        /// <summary>
        /// Gets or sets the patient list of notes.
        /// </summary>
        public virtual ICollection<Note> Notes { get; set; }

        /// <summary>
        /// Gets or sets the patient notes note lines.
        /// </summary>
        public virtual ICollection<NoteLine> NoteLines { get; set; }

        /// <summary>
        /// Gets or sets the list of addresses.
        /// </summary>
        public virtual ICollection<Address> Addresses { get; set; }

        /// <summary>
        /// Gets the validation error based upon the location of the array. 
        /// </summary>
        /// <param name="propertyName">The property being passed in.</param>
        /// <returns>The error message.</returns>
        public string this[string propertyName]
        {
            get
            {
                return this.GetValidationError(propertyName);
            }
        }

        /// <summary>
        /// The get validation error method.
        /// </summary>
        /// <param name="propertyName">The property being passed in.</param>
        /// <returns>The error message for the correct property.</returns>
        public string GetValidationError(string propertyName)
        {
            string result = null;

            switch (propertyName)
            {
                case "FirstName":
                    result = this.ValidateFirstName();
                    break;

                case "LastName":
                    result = this.ValidateLastName();
                    break;

                case "Phone":
                    result = this.ValidatePhoneNumber();
                    break;

                case "Email":
                    result = this.ValidateEmailAddress();
                    break;

                default:
                    break;
            }

            return result;
        }

        /// <summary>
        /// The validate name method.
        /// </summary>
        /// <returns>The name error message.</returns>
        public string ValidateFirstName()
        {
            string result = null;

            if (this.FirstName == null || this.FirstName == string.Empty)
            {
                result = "The first name is required";
            }
            else if (!Regex.IsMatch(this.FirstName, @"^[a-zA-Z]*$"))
            {
                result = "The name can only contain certain characters.";
            }
            else if (this.FirstName.Length > 100)
            {
                result = "The name must be 100 characters or less.";
            }

            return result;
        }

        /// <summary>
        /// The validate name method.
        /// </summary>
        /// <returns>The name error message.</returns>
        public string ValidateLastName()
        {
            string result = null;

            if (this.LastName == null || this.LastName == string.Empty)
            {
                result = "The first name is required";
            }
            else if (!Regex.IsMatch(this.LastName, @"^[a-zA-Z\s]*$"))
            {
                result = "The name can only contain letters.";
            }
            else if (this.LastName.Length > 100)
            {
                result = "The name must be 100 characters or less.";
            }

            return result;
        }

        /// <summary>
        /// The validate name method.
        /// </summary>
        /// <returns>The name error message.</returns>
        public string ValidatePhoneNumber()
        {
            string result = null;

            if (this.Phone == null || this.Phone == string.Empty)
            {
                result = "Phone number should be in the format nnn-nnn-nnnn";
            }
            else if (!Regex.IsMatch(this.Phone, @"^((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}$"))
            {
                result = "The phone number can only contain certain characters.";
            }
            else if (this.FirstName.Length > 100)
            {
                result = "The name must be 100 characters or less.";
            }

            return result;
        }

        /// <summary>
        /// The validate name method.
        /// </summary>
        /// <returns>The name error message.</returns>
        public string ValidateEmailAddress()
        {
            string result = null;

            if (this.Email == null || this.Email == string.Empty)
            {
                result = "Email must be in an email format.";
            }
            else if (!Regex.IsMatch(this.Email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$"))
            {
                result = "Email must be in an email format.";
            }
            else if (this.FirstName.Length > 100)
            {
                result = "The email must be 255 characters or less.";
            }

            return result;
        }

        /// <summary>
        /// Over ride method for the To string method.
        /// </summary>
        /// <returns>Returns a string value of the patient first name.</returns>
        public override string ToString()
        {
            return this.FirstName + " " + this.LastName;
        }
    }
}
