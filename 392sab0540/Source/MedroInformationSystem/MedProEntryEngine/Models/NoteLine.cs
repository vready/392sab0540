﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MedProEntryEngine
{
    /// <summary>
    /// The note line class.
    /// </summary>
    public class NoteLine
    {
        /// <summary>
        /// Gets or sets the note line id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the note line patient note.
        /// </summary>
        [Required]
        public string PatientNote { get; set; }

        /// <summary>
        /// Gets or sets the note line time stamp.
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the note.
        /// </summary>
        public virtual Note Note { get; set; }

        /// <summary>
        /// Gets or sets the note id.
        /// </summary>
        public int NoteId { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is archived is true or not.
        /// </summary>
        public bool IsArchived { get; set; }

        /// <summary>
        /// The over ride string method.
        /// </summary>
        /// <returns>Returns a patient note.</returns>
        public override string ToString()
        {
            return this.PatientNote;
        }
    }
}
