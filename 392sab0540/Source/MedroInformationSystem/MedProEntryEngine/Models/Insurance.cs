﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MedProEntryEngine
{
    /// <summary>
    /// The insurance class.
    /// </summary>
    public class Insurance
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Insurance"/> class.
        /// </summary>
        public Insurance()
        {
            this.Addresses = new List<Address>();
        }

        /// <summary>
        /// The insurance id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the insurance patient id.
        /// </summary>
        public int PatientId { get; set; }

        /// <summary>
        /// Gets or sets the insurance patient.
        /// </summary>
        public virtual Patient Patient { get; set; }

        /// <summary>
        /// Gets or sets the insurance addresses.
        /// </summary>
        public virtual ICollection<Address> Addresses { get; set; }

        /// <summary>
        /// Gets or sets the insurance company name.
        /// </summary>
        [Required]
        [MaxLength(150)]
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the insurance type.
        /// </summary>
        public InsuranceType InsuranceType { get; set; }

        /// <summary>
        /// Gets or sets the policy number.
        /// </summary>
        public string PolicyNumber { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        /// public DateTime StartDate { get; set; }
        [Required]
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is archived is true or not.
        /// </summary>
        public bool IsArchived { get; set; }

        /// <summary>
        /// Gets or sets the patient billing code.
        /// </summary>
        public string PatientBillingCode { get; set; }

        /// <summary>
        /// Override method for the to string.
        /// </summary>
        /// <returns>Returns a modified to string method result.</returns>
        public override string ToString()
        {
            return this.CompanyName;
        }
    }
}
