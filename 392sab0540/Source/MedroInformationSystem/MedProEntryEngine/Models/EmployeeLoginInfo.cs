﻿using System;

namespace MedProEntryEngine
{
    /// <summary>
    /// The employee login info class.
    /// </summary>
    public class EmployeeLoginInfo
    {
        /// <summary>
        /// Gets or sets the id number of the employee login.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the employee id attached to the login info.
        /// </summary>
        public int EmployeeId { get; set; }

        /// <summary>
        /// The employee field.
        /// </summary>
        public Employee Employee { get; set; }

        /// <summary>
        /// Gets or sets the computer ip address.
        /// </summary>
        public string ComputerIpAddress { get; set; }

        /// <summary>
        /// Gets or sets the time stamp of the login.
        /// </summary>
        public DateTime? TimeStamp { get; set; }

        /// <summary>
        /// Gets or sets the value whether is archived is true or false.
        /// </summary>
        public bool IsArchived { get; set; }
    }
}
