﻿using System;
using System.Windows.Input;

namespace MedroInformationSystem
{
    /// <summary>
    /// The delegate command class.
    /// </summary>
    public class DelegateCommand : ICommand
    {
        /// <summary>
        /// The command action property.
        /// </summary>
        private readonly Action<object> command;

        /// <summary>
        /// The can execute field.
        /// </summary>
        private readonly Predicate<object> canExecute;

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
        /// </summary>
        /// <param name="command">The command being passed in.</param>
        public DelegateCommand(Action<object> command) : this(command, null)
        {            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DelegateCommand"/> class.
        /// </summary>
        /// <param name="command">The command being passed in.</param>
        /// <param name="canExecute">The can execute field being passed in.</param>
        public DelegateCommand(Action<object> command, Predicate<object> canExecute)
        {
            if (command == null)
            {
                throw new Exception("Command was null.");
            }

            this.command = command;
            this.canExecute = canExecute;
        }

        /// <summary>
        /// The can execute changed property.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }

            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        /// <summary>
        /// The can execute method.
        /// </summary>
        /// <param name="parameter">The parameter being passed in.</param>
        /// <returns>Returns true or false.</returns>
        public bool CanExecute(object parameter)
        {
            return this.canExecute == null ? true : this.canExecute(parameter);
        }

        /// <summary>
        /// The execute method.
        /// </summary>
        /// <param name="parameter">The parameter being passed in.</param>
        public void Execute(object parameter)
        {
            this.command(parameter);
        }
    }
}
