﻿using System.Windows;

namespace MedroInformationSystem
{
    /// <summary>
    /// Interaction logic for WorkspaceWindow.xaml.
    /// </summary>
    public partial class WorkspaceWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="WorkspaceWindow"/> class.
        /// </summary>
        public WorkspaceWindow()
        {
            this.InitializeComponent();
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }
    }
}
