﻿using System.Windows.Controls;

namespace MedroInformationSystem
{
    /// <summary>
    /// Interaction logic for MultiInsuranceView.xaml.
    /// </summary>
    public partial class MultiInsuranceView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MultiInsuranceView"/> class.
        /// </summary>
        public MultiInsuranceView()
        {
            this.InitializeComponent();
        }
    }
}
