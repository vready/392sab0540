﻿using System.Windows;

namespace MedroInformationSystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml.
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
            this.SizeToContent = SizeToContent.WidthAndHeight;
            this.MaxWidth = 900;
            this.MinWidth = 400;
            this.MaxHeight = 900;
            this.MinHeight = 300;
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;

        }
    }
}
