﻿using System.Windows.Controls;

namespace MedroInformationSystem
{
    /// <summary>
    /// Interaction logic for SelectedItemsView.xaml.
    /// </summary>
    public partial class SelectedItemsView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SelectedItemsView"/> class.
        /// </summary>
        public SelectedItemsView()
        {
            this.InitializeComponent();
        }
    }
}
