﻿using System.Windows.Controls;

namespace MedroInformationSystem
{
    /// <summary>
    /// Interaction logic for PatientView.xaml.
    /// </summary>
    public partial class PatientView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PatientView"/> class.
        /// </summary>
        public PatientView()
        {
            this.InitializeComponent();
        }
    }
}
