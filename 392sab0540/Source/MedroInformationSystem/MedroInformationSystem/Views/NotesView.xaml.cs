﻿using System.Windows.Controls;

namespace MedroInformationSystem
{
    /// <summary>
    /// Interaction logic for NotesView.xaml.
    /// </summary>
    public partial class NotesView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotesView"/> class.
        /// </summary>
        public NotesView()
        {
            this.InitializeComponent();
        }
    }
}
