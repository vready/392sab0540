﻿using MedProDataAccess;
using System.Windows;

namespace MedroInformationSystem
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {        
        /// <summary>
        /// The employee login view model field.
        /// </summary>
        private EmployeeLoginViewModel employeeLoginViewModel;

        /// <summary>
        /// The field that stores that the employee is not null or white space.
        /// </summary>
        private bool employeeIsChecked;

        /// <summary>
        /// The field that stores that the password is not null or white space.
        /// </summary>
        private bool passwordIsChecked;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginWindow"/> class.
        /// </summary>
        /// <param name="mainWindowViewModel">The main window view model being passed in.</param>
        /// <param name="repository">The repository being passed in.</param>
        public LoginWindow(MainWindowViewModel mainWindowViewModel, Repository repository)
        {
            this.InitializeComponent();
            this.employeeLoginViewModel = new EmployeeLoginViewModel(mainWindowViewModel, repository);
            this.employeeIsChecked = false;
            this.passwordIsChecked = false;
            this.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
        }

        /// <summary>
        /// The login button click method.
        /// </summary>
        /// <param name="sender">The sender being passed in.</param>
        /// <param name="e">The event being passed in.</param>
        private void loginButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(this.employeeNumber.Text))
            {
                this.employeeIsChecked = true;
            }

            if (!string.IsNullOrWhiteSpace(this.passwordBox.Password))
            {
                this.passwordIsChecked = true;
            }

            if (this.employeeIsChecked && this.passwordIsChecked)
            {
               bool result = this.employeeLoginViewModel.LoginCredentialPassthrough(this.employeeNumber, this.passwordBox);

                if (result)
                {
                    this.DialogResult = true;
                }
                else
                {
                    this.passwordBox.Password = string.Empty;
                    this.employeeNumber.Text = string.Empty;
                }               
            }
            else
            {
                MessageBox.Show("Employee number and/or password must not be empty.");
                this.passwordBox.Password = string.Empty;
                this.employeeNumber.Text = string.Empty;
            }            
        }
    }
}
