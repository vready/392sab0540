﻿using System.Windows.Controls;

namespace MedroInformationSystem
{
    /// <summary>
    /// Interaction logic for MultiNotesView.xaml.
    /// </summary>
    public partial class MultiNotesView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MultiNotesView"/> class.
        /// </summary>
        public MultiNotesView()
        {
            this.InitializeComponent();
        }
    }
}
