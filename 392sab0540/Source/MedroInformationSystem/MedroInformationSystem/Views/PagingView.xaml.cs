﻿using System.Windows.Controls;

namespace MedroInformationSystem
{
    /// <summary>
    /// Interaction logic for PagingView.xaml.
    /// </summary>
    public partial class PagingView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PagingView"/> class.
        /// </summary>
        public PagingView()
        {
            this.InitializeComponent();
        }
    }
}
