﻿using System.Windows;
using System.Windows.Controls;

namespace MedroInformationSystem
{
    /// <summary>
    /// Interaction logic for MultiPatientView.xaml.
    /// </summary>
    public partial class MultiPatientView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MultiPatientView"/> class.
        /// </summary>
        public MultiPatientView()
        {
            this.InitializeComponent();
        }
    }
}
