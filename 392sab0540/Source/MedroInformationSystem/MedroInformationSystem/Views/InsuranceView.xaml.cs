﻿using System.Windows.Controls;

namespace MedroInformationSystem
{
    /// <summary>
    /// Interaction logic for InsuranceView.xaml.
    /// </summary>
    public partial class InsuranceView : UserControl
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InsuranceView"/> class.
        /// </summary>
        public InsuranceView()
        {
           this.InitializeComponent();
        }
    }
}
