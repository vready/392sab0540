﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ComponentModel;
using System.Windows;
using MedProDataAccess;
using MedProEntryEngine;

namespace MedroInformationSystem
{
    /// <summary>
    /// The patient view model class.
    /// </summary>
    public class PatientViewModel : WorkspaceViewModel, IDataErrorInfo
    {
        /// <summary>
        /// The patient view model patient field.
        /// </summary>
        private Patient patient;

        /// <summary>
        /// View model's repository.
        /// </summary>
        private Repository repository;

        /// <summary>
        /// The save command field.
        /// </summary>
        private ICommand saveCommand;

        /// <summary>
        /// The is selected field.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// The multi notes view model field.
        /// </summary>
        private MultiNotesViewModel filteredNotesViewModel;

        /// <summary>
        /// The filtered insurance view model field.
        /// </summary>
        private MultiInsuranceViewModel filteredInsuranceViewModel;
        
        /// <summary>
        /// The flitered address view model field.
        /// </summary>
        private MultiAddressViewModel filteredAddressViewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="PatientViewModel"/> class.
        /// </summary>
        /// <param name="patient">The patient being passed in.</param>
        /// <param name="repository">The repository being passed in.</param>
        public PatientViewModel(Patient patient, Repository repository)
            : base("Patient")
        {
            this.repository = repository;
            this.patient = patient;            
            this.filteredNotesViewModel = new MultiNotesViewModel(this.repository, this.patient);
            this.filteredNotesViewModel.AllNotes = this.FilteredNotes;
            this.filteredInsuranceViewModel = new MultiInsuranceViewModel(this.repository, this.patient);
            this.filteredInsuranceViewModel.AllInsurances = this.FilteredInsurance;
            this.filteredAddressViewModel = new MultiAddressViewModel(this.repository, this.patient);
            this.filteredAddressViewModel.AllAddresses = this.FilteredAddresses;
        }

        /// <summary>
        /// Gets the error string for the product.
        /// </summary>
        public string Error
        {
            get
            {
                return this.patient.Error;
            }
        }

        /// <summary>
        /// Gets the filtered notes.
        /// </summary>
        public MultiNotesViewModel FilteredNotesViewModel
        {
            get
            {
                return this.filteredNotesViewModel;
            }
        }  

        /// <summary>
        /// Gets the value of the filtered insurance view model property.
        /// </summary>
        public MultiInsuranceViewModel FilteredInsuranceViewModel
        {
            get
            {
                return this.filteredInsuranceViewModel;
            }
        }

        /// <summary>
        /// Gets the value of the filtered address view model property.
        /// </summary>
        public MultiAddressViewModel FilteredAddressViewModel
        {
            get
            {
                return this.filteredAddressViewModel;
            }
        }

        /// <summary>
        /// Gets the filtered notes of the patient.
        /// </summary>
        public ObservableCollection<NotesViewModel> FilteredNotes
        {
            get
            {
                var notes =
                    (from n in this.patient.Notes
                    select new NotesViewModel(n, this.repository)).ToList();

                this.FilteredNotesViewModel.AddPropertyChangedEvent(notes);
                return new ObservableCollection<NotesViewModel>(notes);
            }
        }

        /// <summary>
        /// Gets the value of the filtered insurance property.
        /// </summary>
        public ObservableCollection<InsuranceViewModel> FilteredInsurance
        {
            get
            {
                var insurances =
                    (from i in this.patient.Insurances
                     select new InsuranceViewModel(i, this.repository)).ToList();

                this.FilteredInsuranceViewModel.AddPropertyChangedEvent(insurances);
                return new ObservableCollection<InsuranceViewModel>(insurances);
            }
        }

        /// <summary>
        /// Gets the value of the filtered addresses property.
        /// </summary>
        public ObservableCollection<AddressViewModel> FilteredAddresses
        {
            get
            {
                var addresses =
                    (from a in this.patient.Addresses
                     select new AddressViewModel(a, this.repository)).ToList();

                this.FilteredAddressViewModel.AddPropertyChangedEvent(addresses);
                return new ObservableCollection<AddressViewModel>(addresses);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is selected is true or false.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
            }
        }

        /// <summary>
        /// Gets the list of current addresses.
        /// </summary>
        public string CurrentAddress
        {
            get
            {
                try
                {
                    var address =
                        (from a in this.patient.Addresses
                         where a.EndDate == null
                         select a.ToString()).First();

                    //(from x in Items where x.Id == 123 select x).First();
                    return address;
                }
                catch
                {
                    return "No address on file";
                }
            }
        }

        /// <summary>
        /// Gets the save command property.
        /// </summary>
        public ICommand SaveCommand
        {
            get
            {
                return this.saveCommand = new DelegateCommand(p => this.Save());
            }
        }

        /// <summary>
        /// Gets or sets the patient first name.
        /// </summary>
        public string FirstName
        {
            get
            {
                return this.patient.FirstName;
            }

            set
            {
                this.patient.FirstName = value;
                this.OnPropertyChanged("FirstName");
            }
        }

        /// <summary>
        /// Gets or sets the patient middle name.
        /// </summary>
        public string MiddleName
        {
            get
            {
                return this.patient.MiddleName;
            }

            set
            {
                this.patient.MiddleName = value;
                this.OnPropertyChanged("MiddleName");
            }
        }

        /// <summary>
        /// Gets or sets the patient last name.
        /// </summary>
        public string LastName
        {
            get
            {
                return this.patient.LastName;
            }

            set
            {
                this.patient.LastName = value;
                this.OnPropertyChanged("LastName");
            }
        }

        /// <summary>
        /// Gets or sets the patient date of birth.
        /// </summary>
        public DateTime? DateOfBirth
        {
            get
            {
                return this.patient.DateOfBirth;
            }

            set
            {
                this.patient.DateOfBirth = value;
                this.OnPropertyChanged("DateOfBirth");
                this.OnPropertyChanged("IsMinor");
            }
        }

        /// <summary>
        /// Gets or sets the patient phone number.
        /// </summary>
        public string Phone
        {
            get
            {
                return this.patient.Phone;
            }

            set
            {
                this.patient.Phone = value;
                this.OnPropertyChanged("Phone");
            }
        }

        /// <summary>
        /// Gets or sets the patient email address.
        /// </summary>
        public string Email
        {
            get
            {
                return this.patient.Email;
            }

            set
            {
                this.patient.Email = value;
                this.OnPropertyChanged("Email");
            }
        }

        /// <summary>
        /// Gets or sets the patient gender.
        /// </summary>
        public Gender Gender
        {
            get
            {
                return this.patient.Gender;
            }

            set
            {
                this.patient.Gender = value;
                this.OnPropertyChanged("Gender");
            }
        }

        /// <summary>
        /// Gets the patient gender.
        /// </summary>
        public IEnumerable<Gender> Genders
        {
            get
            {
                return Enum.GetValues(typeof(Gender)) as IEnumerable<Gender>;
            }
        }

        /// <summary>
        /// Gets or sets the patient medical record number.
        /// </summary>
        public int MedicalRecordNumber
        {
            get
            {
                return 789456123 + this.patient.Id;
            }

            set
            {
                this.patient.MedicalRecordNumber = value;
                this.OnPropertyChanged("MedicalRecordNumber");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the patient is a minor.
        /// </summary>
        public bool IsMinor
        {
            get
            {
                if (this.patient.DateOfBirth != null)
                {
                    DateTime? date1 = this.patient.DateOfBirth;
                    DateTime? date2 = DateTime.Today;
                    TimeSpan timespan = date2.Value.Subtract(date1.Value);
                    if (Math.Round(timespan.Days / 365.25, 2) < 18)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }

            set
            {
                this.IsMinor = value;
                this.OnPropertyChanged("IsMinor");
            }
        }

        /// <summary>
        /// Gets the patient. 
        /// </summary>
        public Patient Patient
        {
            get
            {
                return this.patient;
            }
        }

        /// <summary>
        /// Gets the property to set the error message.
        /// </summary>
        /// <param name="propertyName">The property being passed in.</param>
        /// <returns>The error message of the property being checked.</returns>
        public string this[string propertyName]
        {
            get
            {
                return this.patient[propertyName];
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is not a new patient is true or false.
        /// </summary>
        public bool IsNotNewPatient { get; set; }

        /// <summary>
        /// The method that saves a product to a repository.
        /// </summary>
        /// <returns>A true or a false.</returns>
        public bool Save()
        {
            bool result = true;

            if (this.patient.IsValid)
            {
                this.patient.IsNotNewPatient = true;
                this.repository.AddPatient(this.patient);
                this.repository.SaveToDatabase();
            }
            else
            {
                result = false;
                MessageBox.Show("All fields must be filed out properly before saving patient.");
            }

            return result;
        }

        /// <summary>
        /// A method that creates commands.
        /// </summary>
        protected override void CreateCommands()
        {
            this.Commands.Add(new CommandViewModel("OK", new DelegateCommand(p => this.OkExecute())));
            this.Commands.Add(new CommandViewModel("Cancel", new DelegateCommand(p => this.CancelExecute())));
        }

        /// <summary>
        /// This is the ok execute method.
        /// </summary>
        private void OkExecute()
        {
            if (this.Save())
            {
                this.CloseAction(true);
            }
        }

        /// <summary>
        /// This is the cancel execute method.
        /// </summary>
        private void CancelExecute()
        {
            if (!this.patient.IsNotNewPatient)
            {
                this.repository.RemovePatient(this.Patient);
            }

            this.CloseAction(false);
        }        
    }
}
