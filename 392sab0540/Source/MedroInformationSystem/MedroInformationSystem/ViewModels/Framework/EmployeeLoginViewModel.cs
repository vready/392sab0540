﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using MedProDataAccess;
using MedProEntryEngine;

namespace MedroInformationSystem
{
    /// <summary>
    /// The employee login view model class.
    /// </summary>
    public class EmployeeLoginViewModel : WorkspaceViewModel
    {
        /// <summary>
        /// Field to gain access to the view model to set the field so that we can disable the commands until after login.
        /// </summary>
        private MainWindowViewModel mainWindowViewModel;

        /// <summary>
        /// The employee number field.
        /// </summary>
        private string employeeNumber;

        /// <summary>
        /// The employee password field.
        /// </summary>
        private string employeePassword;

        /// <summary>
        /// The repository being passed in.
        /// </summary>
        private Repository repository;

        /// <summary>
        /// The number of tries field.
        /// </summary>
        private int numberOfTries;

        /// <summary>
        /// The system lock field.
        /// </summary>
        private bool systemLock;       

        /// <summary>
        /// Initializes a new initializes a new instance of the <see cref="EmployeeLoginViewModel"/> class.
        /// </summary>
        /// <param name="mainWindowViewModel">The main window view model being passed in.</param>
        /// <param name="repository">The repository being passed in.</param>
        public EmployeeLoginViewModel(MainWindowViewModel mainWindowViewModel, Repository repository)
            : base("Employee Login")
        {
            this.repository = repository;
            this.SystemLocked = false;
            this.mainWindowViewModel = mainWindowViewModel;
            this.numberOfTries = 1;
        }

        /// <summary>
        /// Gets or sets the employee number.
        /// </summary>
        public string EmployeeNumber
        {
            get
            {
                return this.employeeNumber;
            }
            set
            {
                this.employeeNumber = value;
                this.OnPropertyChanged("EmployeeNumber");
            }
        }

        /// <summary>
        /// Gets or sets the employee password property.
        /// </summary>
        public string EmployeePassword
        {
            get
            {
                return this.employeePassword;
            }
            set
            {
                this.employeePassword = value;
                this.OnPropertyChanged("EmployeePassword");
            }
        }

        /// <summary>
        /// Gets or sets the number of tries.
        /// </summary>
        public int NumberOfTries
        {
            get
            {
                return this.numberOfTries;
            }
            set
            {
                this.numberOfTries = value;
            }
        }

        /// <summary>
        /// Gets or sets the system locked property.
        /// </summary>
        public bool SystemLocked
        {
            get
            {
                return this.systemLock;
            }
            set
            {
                this.systemLock = value;
            }
        }

        /// <summary>
        /// The create commands method.
        /// </summary>
        protected override void CreateCommands()
        {
            this.Commands.Add(new CommandViewModel("Submit", new DelegateCommand(p => this.CheckLoginInfo(this.EmployeeNumber, this.EmployeePassword), p => this.SystemLocked == false)));
        }

        /// <summary>
        /// The eventual method to check to see if the username and the password match.
        /// </summary>
        /// <param name="employeeNumber">The employee number being passed in.</param>
        /// <param name="password">The password being passed in.</param>
        /// <returns>Return a true or false depending on if username and password match.</returns>
        public bool CheckLoginInfo(string employeeNumber, string employeePassword)
        {
            bool result = false;

            if (this.CheckCredentials(this.EmployeeNumber, this.EmployeePassword))
            {
                /// Sets the switch to true so that the main window view model buttons will be active.
                this.mainWindowViewModel.EmployeeValid = true;               

                /// Sets the employee login information.
                EmployeeLoginInfo employeeLoginInfo = new EmployeeLoginInfo();
                employeeLoginInfo.Employee = this.FindEmployee(employeeNumber, employeePassword);
                employeeLoginInfo.ComputerIpAddress = this.GetIpAddress();
                employeeLoginInfo.TimeStamp = DateTime.Now;

                employeeLoginInfo.Employee.EmployeeLogins.Add(employeeLoginInfo);         

                /// Adds the employee and their login data to the repository.
                this.repository.AddEmployeeLoginInfo(employeeLoginInfo);
                this.repository.SaveToDatabase();
                result = true;
            }
            else if (!this.CheckCredentials(this.EmployeeNumber, this.EmployeePassword))
            {
                MessageBox.Show("Sorry user name and/or password is incorrect.");
                this.NumberOfTries++;                

                if (this.NumberOfTries > 3)
                {
                    MessageBox.Show("You have exceeded the number of attempts allowed. Please contact your administrator.");
                    this.SystemLocked = true;
                }
            }

            return result;
        }

        /// <summary>
        /// The login credential passthrough method.
        /// </summary>
        /// <param name="textBox">The login textbox.</param>
        /// <param name="passwordBox">The password text box.</param>
        public bool LoginCredentialPassthrough(TextBox textBox, PasswordBox passwordBox)
        {
            bool result;

            this.EmployeeNumber = textBox.Text;
            this.EmployeePassword = passwordBox.Password;
            result = this.CheckLoginInfo(this.EmployeeNumber, this.EmployeePassword);

            return result;
        }

        /// <summary>
        /// The find employee method.
        /// </summary>
        /// <param name="employeeNumber">The employee number being passed in.</param>
        /// <param name="passWord">The password being passed in.</param>
        /// <returns>Returns an employee.</returns>
        private Employee FindEmployee(string employeeNumber, string passWord)
        {
            return this.repository.GetEmployee(employeeNumber, passWord);
        }

        /// <summary>
        /// The check credentials method.
        /// </summary>
        /// <param name="employeeNumber">The employee number being passed in.</param>
        /// <param name="employeePassword">The employee password being passed in.</param>
        /// <returns>Returns a true or false if employee is found.</returns>
        private bool CheckCredentials(string employeeNumber, string employeePassword)
        {
            bool loginValid = false;

            if (this.FindEmployee(employeeNumber, employeePassword) != null)
            {
                loginValid = true;
            }

            return loginValid;
        }

        /// <summary>
        /// Method to get the ip address.
        /// </summary>
        /// <returns>Returns the ip address.</returns>
        private string GetIpAddress()
        {
            string hostName = Dns.GetHostName();
            string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();
            return myIP;
        }               
    }
}
