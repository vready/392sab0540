﻿using System;
using System.Diagnostics.Contracts;
using MedProEntryEngine;

namespace MedroInformationSystem
{
    /// <summary>
    /// The paging view model class.
    /// </summary>
    public class PagingViewModel : ViewModel
    {
        /// <summary>
        /// The item count field.
        /// </summary>
        private int patientCount;

        /// <summary>
        /// The current page field.
        /// </summary>
        private int currentPage;

        /// <summary>
        /// The page size field.
        /// </summary>
        private int pageSize;

        /// <summary>
        /// Initializes a new instance of the <see cref="PagingViewModel"/> class.
        /// </summary>
        /// <param name="patientCount">The item count being passed in.</param>
        public PagingViewModel(int patientCount)
            : base(string.Empty)
        {
            Contract.Requires(patientCount >= 0);
            Contract.Requires(this.pageSize > 0);
            this.PatientCount = patientCount;
            this.PageSize = 5;

            if (this.PatientCount == 0)
            {
                this.CurrentPage = 0;
            }
            else
            {
                this.CurrentPage = 1;
            }

            this.GoToFirstPageCommand = new DelegateCommand(p => this.CurrentPage = 1, p => this.PatientCount > 0 && this.CurrentPage > 1);
            this.GoToPreviousPageCommand = new DelegateCommand(p => this.CurrentPage--, p => this.PatientCount > 0 && this.CurrentPage > 1);
            this.GoToNextPageCommand = new DelegateCommand(p => this.CurrentPage++, p => this.PatientCount > 0 && this.CurrentPage < this.PageCount);
            this.GoToLastPageCommand = new DelegateCommand(p => this.CurrentPage = this.PageCount, p => this.PatientCount > 0 && this.CurrentPage < this.PageCount);
        }

        /// <summary>
        /// Gets or sets the value of current page changed. 
        /// </summary>
        public EventHandler<CurrentPageChangeEventArgs> CurrentPageChanged { get; set; }

        /// <summary>
        /// Gets or sets the to to first page.
        /// </summary>
        public DelegateCommand GoToFirstPageCommand { get; set; }

        /// <summary>
        /// Gets or sets the go to previous page.
        /// </summary>
        public DelegateCommand GoToPreviousPageCommand { get; set; }

        /// <summary>
        /// Gets or sets the got to next page.
        /// </summary>
        public DelegateCommand GoToNextPageCommand { get; set; }

        /// <summary>
        /// Gets or sets the got to last page.
        /// </summary>
        public DelegateCommand GoToLastPageCommand { get; set; }

        /// <summary>
        /// Gets or sets the item count.
        /// </summary>
        public int PatientCount
        {
            get
            {
                return this.patientCount;
            }

            set
            {
                this.patientCount = value;
                this.OnPropertyChanged("PageSize");
                this.OnPropertyChanged("PatientCount");
                this.OnPropertyChanged("PageCount");
            }
        }

        /// <summary>
        /// Gets or sets the page size.
        /// </summary>
        public int PageSize
        {
            get
            {
                return this.pageSize;
            }

            set
            {
                this.pageSize = value;
                this.OnPropertyChanged("PageSize");
                this.OnPropertyChanged("PatientCount");
                this.OnPropertyChanged("PageCount");
            }
        }

        /// <summary>
        /// Gets the value of the page count.
        /// </summary>
        public int PageCount
        {
            get
            {
                return (int)Math.Ceiling((double)this.patientCount / this.pageSize);
            }
        }

        /// <summary>
        /// Gets or sets the current page.
        /// </summary>
        public int CurrentPage
        {
            get
            {
                return this.currentPage;
            }

            set
            {
                this.currentPage = value;
                this.OnPropertyChanged("CurrentPage");
                var e = this.CurrentPageChanged;

                if (e != null)
                {
                    this.CurrentPageChanged(this, new CurrentPageChangeEventArgs(this.CurrentPageStartIndex, this.PageSize));
                }
            }
        }

        /// <summary>
        /// Gets the value of the current page start index.
        /// </summary>
        public int CurrentPageStartIndex
        {
            get
            {
                return this.PageCount == 0 ? -1 : (this.CurrentPage - 1) * this.PageSize;
            }
        }
    }
}
