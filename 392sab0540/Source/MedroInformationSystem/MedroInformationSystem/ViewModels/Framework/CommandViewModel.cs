﻿using System;
using System.Windows.Input;

namespace MedroInformationSystem
{
    /// <summary>
    /// The Command view model class.
    /// </summary>
    public class CommandViewModel : ViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CommandViewModel"/> class.
        /// </summary>
        /// <param name="displayName">The name of the display.</param>
        /// <param name="command">The command being passed in.</param>
        public CommandViewModel(string displayName, ICommand command)
           : base(displayName)
        {
            if (command == null)
            {
                throw new Exception("Command was null.");
            }

            this.Command = command;
        }

        /// <summary>
        /// Gets or sets the command property.
        /// </summary>
        public ICommand Command { get; set; }
    }
}
