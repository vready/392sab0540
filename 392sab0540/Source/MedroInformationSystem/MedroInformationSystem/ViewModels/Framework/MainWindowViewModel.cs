﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows.Data;
using MedProDataAccess;
using MedProEntryEngine;

namespace MedroInformationSystem
{
    /// <summary>
    /// The main window view model class.
    /// </summary>
    public class MainWindowViewModel : WorkspaceViewModel
    {
        /// <summary>
        /// The view models field.
        /// </summary>
        private ObservableCollection<WorkspaceViewModel> viewModels;

        /// <summary>
        /// The main window repository field.
        /// </summary>
        private Repository repository;

        /// <summary>
        /// The employee valid field.
        /// </summary>
        private bool employeeValid;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindowViewModel"/> class.
        /// </summary>
        public MainWindowViewModel()
            : base("Medpro - Patient Management System")
        {
            this.repository = new Repository();

            LoginWindow loginWindow = new LoginWindow(this, this.repository);

            if(loginWindow.ShowDialog() == true)
            {
                loginWindow.Close();
                this.ShowAllPatients();
            }           
        }

        /// <summary>
        /// Gets the list of view models.
        /// </summary>
        public ObservableCollection<WorkspaceViewModel> ViewModels
        {
            get
            {
                if (this.viewModels == null)
                {
                    this.viewModels = new ObservableCollection<WorkspaceViewModel>();
                }

                return this.viewModels;
            }
        }

        /// <summary>
        /// Gets or setsthe employee valid field.
        /// </summary>
        public bool EmployeeValid
        {
            get
            {
                return this.employeeValid;
            }
            set
            {
                this.employeeValid = value;
            }
        }

        /// <summary>
        /// This method creates the views for the user.
        /// </summary>
        public void CreateViews()
        {
            this.ShowAllPatients();
        }


        /// <summary>
        /// The create commands method.
        /// </summary>
        protected override void CreateCommands()
        {
            this.Commands.Add(new CommandViewModel("View all patients", new DelegateCommand(p => this.ShowAllPatients(), p => this.EmployeeValid == true)));
            this.Commands.Add(new CommandViewModel("View all insurances", new DelegateCommand(p => this.ShowAllInsurances(), p => this.EmployeeValid == true)));
            this.Commands.Add(new CommandViewModel("View all visits", new DelegateCommand(p => this.ShowAllNotes(), p => this.EmployeeValid == true)));
            this.Commands.Add(new CommandViewModel("View all addresses", new DelegateCommand(p => this.ShowAllAddresses(), p => this.EmployeeValid == true)));
            this.Commands.Add(new CommandViewModel("Employee reports", new DelegateCommand(p => this.ViewReports(), p => this.EmployeeValid == true)));
        }

        /// <summary>
        /// The activate view model method.
        /// </summary>
        /// <param name="viewModel">The view model being passed in.</param>
        private void ActivateViewModel(WorkspaceViewModel viewModel)
        {
            ICollectionView collectionView = CollectionViewSource.GetDefaultView(this.viewModels);

            if (collectionView != null)
            {
                collectionView.MoveCurrentTo(viewModel);
            }
        }

        /// <summary>
        /// The on workspace request close method.
        /// </summary>
        /// <param name="sender">The sender sending the object.</param>
        /// <param name="e">The event being sent.</param>
        private void OnWorkspaceRequestClose(object sender, EventArgs e)
        {
            this.ViewModels.Remove(sender as WorkspaceViewModel);
        }

        /// <summary>
        /// The show all patients method.
        /// </summary>
        public void ShowAllPatients()
        {
            MultiPatientViewModel viewModel = this.ViewModels.FirstOrDefault(vm => vm is MultiPatientViewModel) as MultiPatientViewModel;
            if (viewModel == null)
            {
                viewModel = new MultiPatientViewModel(this.repository);
                viewModel.RequestClose += this.OnWorkspaceRequestClose;
                this.ViewModels.Add(viewModel);
            }

            this.ActivateViewModel(viewModel);
        }

        /// <summary>
        /// The show all insurance method.
        /// </summary>
        private void ShowAllInsurances()
        {
            MultiInsuranceViewModel viewModel = this.ViewModels.FirstOrDefault(vm => vm is MultiInsuranceViewModel) as MultiInsuranceViewModel;
            if (viewModel == null)
            {
                viewModel = new MultiInsuranceViewModel(this.repository, null);
                viewModel.RequestClose += this.OnWorkspaceRequestClose;
                this.ViewModels.Add(viewModel);
            }

            this.ActivateViewModel(viewModel);
        }

        /// <summary>
        /// The show all notes method.
        /// </summary>
        private void ShowAllNotes()
        {
            MultiNotesViewModel viewModel = this.ViewModels.FirstOrDefault(vm => vm is MultiNotesViewModel) as MultiNotesViewModel;
            if (viewModel == null)
            {
                viewModel = new MultiNotesViewModel(this.repository, null);
                viewModel.RequestClose += this.OnWorkspaceRequestClose;
                this.ViewModels.Add(viewModel);
            }

            this.ActivateViewModel(viewModel);
        }

        private void ShowAllAddresses()
        {
            MultiAddressViewModel viewModel = this.ViewModels.FirstOrDefault(vm => vm is MultiAddressViewModel) as MultiAddressViewModel;
            if (viewModel == null)
            {
                viewModel = new MultiAddressViewModel(this.repository, null as Patient);
                viewModel.RequestClose += this.OnWorkspaceRequestClose;
                this.ViewModels.Add(viewModel);
            }

            this.ActivateViewModel(viewModel);
        }

        /// <summary>
        /// The show employee login method.
        /// </summary>
        private void ShowEmployeeLogin()
        {
            EmployeeLoginViewModel viewModel = this.ViewModels.FirstOrDefault(vm => vm is EmployeeLoginViewModel) as EmployeeLoginViewModel;
            if (viewModel == null)
            {
                viewModel = new EmployeeLoginViewModel(this, this.repository);
                viewModel.RequestClose += this.OnWorkspaceRequestClose;
                this.ViewModels.Add(viewModel);
            }

            this.ActivateViewModel(viewModel);
        }

        /// <summary>
        /// The method that shows all the locations in a repository.
        /// </summary>
        public void ViewReports()
        {
            ReportViewModel reportViewModel = new ReportViewModel(this.repository);
            reportViewModel.RequestClose += this.OnWorkspaceRequestClose;
            this.viewModels.Add(reportViewModel);

            this.ActivateViewModel(reportViewModel);  
        }
    }
}
