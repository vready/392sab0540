﻿using System.ComponentModel;

namespace MedroInformationSystem
{
    /// <summary>
    /// The view model class.
    /// </summary>
    public abstract class ViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModel"/> class.
        /// </summary>
        /// <param name="displayName">The display name of the view model.</param>
        public ViewModel(string displayName)
        {
            this.DisplayName = displayName;
        }

        /// <summary>
        /// The event handler for a property change. 
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Gets the property of Display name.
        /// </summary>
        public string DisplayName { get; private set; }

        /// <summary>
        /// The on property change when a value in a property is changed.
        /// </summary>
        /// <param name="propertyName">The property being changed.</param>
        protected virtual void OnPropertyChanged(string propertyName)
        {
            var handler = this.PropertyChanged;

            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
