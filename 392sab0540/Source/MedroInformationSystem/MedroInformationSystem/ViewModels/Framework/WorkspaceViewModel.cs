﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace MedroInformationSystem
{
    /// <summary>
    /// The workspace view model class.
    /// </summary>
    public abstract class WorkspaceViewModel : ViewModel
    {
        /// <summary>
        /// The close command field.
        /// </summary>
        private DelegateCommand closeCommand;

        /// <summary>
        /// The commands list field.
        /// </summary>
        private ObservableCollection<CommandViewModel> commands = new ObservableCollection<CommandViewModel>();

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkspaceViewModel"/> class.
        /// </summary>
        /// <param name="displayName">The view display name.</param>
        public WorkspaceViewModel(string displayName)
            : base(displayName)
        {
            this.CreateCommands();
        }

        /// <summary>
        /// The request close event handler.
        /// </summary>
        public event EventHandler RequestClose;

        /// <summary>
        /// Gets or sets the close action property.
        /// </summary>
        public Action<bool> CloseAction { get; set; }

        /// <summary>
        /// Gets the list of commands.
        /// </summary>
        public ObservableCollection<CommandViewModel> Commands
        {
            get
            {
                return this.commands;
            }
        }

        /// <summary>
        /// Gets the command with an attached delegate command.
        /// </summary>
        public ICommand CloseCommand
        {
            get
            {
                if (this.closeCommand == null)
                {
                    this.closeCommand = new DelegateCommand(p => this.OnRequestClose());
                }

                return this.closeCommand;
            }
        }

        /// <summary>
        /// The create commands method.
        /// </summary>
        protected abstract void CreateCommands();

        /// <summary>
        /// The on request close method.
        /// </summary>
        private void OnRequestClose()
        {
            if (this.RequestClose != null)
            {
                this.RequestClose(this, EventArgs.Empty);
            }
        }
    }
}