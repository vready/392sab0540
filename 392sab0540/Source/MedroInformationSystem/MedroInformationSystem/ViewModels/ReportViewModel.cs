﻿using System.Linq;
using MedProDataAccess;


namespace MedroInformationSystem
{
    /// <summary>
    /// The report view model class.
    /// </summary>
    public class ReportViewModel : WorkspaceViewModel
    {
        /// <summary>
        /// The repository being passed in.
        /// </summary>
        private Repository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportViewModel"/> class.
        /// </summary>
        /// <param name="repository">The repository being passed in.</param>
        public ReportViewModel(Repository repository)
            : base ("Employee Login Report")
        {
            this.repository = repository;
            this.LoadReport();
        }

        /// <summary>
        /// The employee logins proprety.
        /// </summary>
        public object EmployeeLogins { get; set; }

        /// <summary>
        /// The create commands method.
        /// </summary>
        protected override void CreateCommands()
        {
            // throw new System.NotImplementedException();
        }

        /// <summary>
        /// The load report method.
        /// </summary>
        private void LoadReport()
        {
            var employees =
                 from e in this.repository.GetEmployee()
                 select new { Name = e.FirstName + " " + e.LastName, Logins = e.EmployeeLogins };

            this.EmployeeLogins = employees;            
        }
    }
}
