﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using MedProDataAccess;
using MedProEntryEngine;

namespace MedroInformationSystem
{
    /// <summary>
    /// The multi insurance view model class.
    /// </summary>
    public class MultiInsuranceViewModel : WorkspaceViewModel
    {
        /// <summary>
        /// The multi insurance view model repository.
        /// </summary>
        private Repository repository;

        private Patient patient;

        /// <summary>
        /// Initializes a new instance of the <see cref="MultiInsuranceViewModel"/> class.
        /// </summary>
        /// <param name="repository">The repository being passed in.</param>
        public MultiInsuranceViewModel(Repository repository, Patient patient)
            : base("All insurances")
        {
            this.repository = repository;
            this.patient = patient;
            this.Commands.Clear();
            this.CreateCommands();
            this.repository.InsuranceAdded += this.OnInsuranceAdded;
            this.repository.InsuranceRemoved += this.OnInsuranceRemoved;

            List<InsuranceViewModel> insurances =
                (from i in this.repository.GetInsurance()
                 select new InsuranceViewModel(i, this.repository)).ToList();

            this.AddPropertyChangedEvent(insurances);            
            this.AllInsurances = new ObservableCollection<InsuranceViewModel>(insurances);
        }

        /// <summary>
        /// The method that adds an event to a property change.
        /// </summary>
        /// <param name="insurances">The insurances being passed in.</param>
        public void AddPropertyChangedEvent(List<InsuranceViewModel> insurances)
        {
            insurances.ForEach(ivm => ivm.PropertyChanged += this.OnInsuranceViewModelPropertyChanged);
        }

        /// <summary>
        /// Gets a value indicating whether is selected is true or false.
        /// </summary>
        public int NumberOfItemsSelected
        {
            get
            {
                return this.AllInsurances.Count(nvm => nvm.IsSelected);
            }
        }

        /// <summary>
        /// Gets or sets the all insurances list.
        /// </summary>
        public ObservableCollection<InsuranceViewModel> AllInsurances { get; set; }

        /// <summary>
        /// The create commands method.
        /// </summary>
        protected override void CreateCommands()
        {
            if (this.patient != null)
            {
                this.Commands.Add(new CommandViewModel("New...", new DelegateCommand(p => this.CreateNewInsuranceExecute())));
                this.Commands.Add(new CommandViewModel("Edit...", new DelegateCommand(p => this.EditInsuranceExecute(), p => this.NumberOfItemsSelected == 1)));
                this.Commands.Add(new CommandViewModel("Delete...", new DelegateCommand(p => this.DeleteInsuranceExecute(), p => this.NumberOfItemsSelected == 1)));
            }
        }

        /// <summary>
        /// The view property event handler change method.
        /// </summary>
        /// <param name="sender">The object being sent.</param>
        /// <param name="e">The event being sent.</param>
        private void OnInsuranceViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsSelected")
            {
                this.OnPropertyChanged("NumberOfItemsSelected");
            }
        }

        /// <summary>
        /// The on insurance added method.
        /// </summary>
        /// <param name="sender">The object being sent.</param>
        /// <param name="e">The event being sent.</param>
        private void OnInsuranceAdded(object sender, InsuranceEventArgs e)
        {
            InsuranceViewModel viewModel = new InsuranceViewModel(e.Insurance, this.repository);
            if(this.patient == null || viewModel.Patient == e.Insurance.Patient)
            {
                viewModel.PropertyChanged += this.OnInsuranceViewModelPropertyChanged;
                this.AllInsurances.Add(viewModel);  
                this.OnPropertyChanged("AllInsurances");
            }
        }

        /// <summary>
        /// The create new insurance execute method.
        /// </summary>
        private void CreateNewInsuranceExecute()
        {
            if (this.patient != null)
            {
                this.repository.AddPatient(this.patient);
            }

            InsuranceViewModel viewModel = new InsuranceViewModel(new Insurance() {Patient = this.patient }, this.repository);
            this.ShowInsurance(viewModel);
        }
        
        /// <summary>
        /// The edit insurance execute method.
        /// </summary>
        private void EditInsuranceExecute()
        {
            try
            {
                InsuranceViewModel viewModel = this.AllInsurances.SingleOrDefault(vm => vm.IsSelected);

                if (viewModel != null)
                {
                    this.ShowInsurance(viewModel);

                    List<InsuranceViewModel> insurances =
                    (from i in this.repository.GetInsurance()
                     where i.PatientId == this.patient.Id
                    select new InsuranceViewModel(i, this.repository)).ToList();

                    this.AddPropertyChangedEvent(insurances);
                    this.AllInsurances = new ObservableCollection<InsuranceViewModel>(insurances);
                    this.OnPropertyChanged("AllInsurances");

                    this.repository.SaveToDatabase();
                }
                else
                {
                    MessageBox.Show("Please select only one customer to edit.");
                }
            }
            catch
            {
                MessageBox.Show("Please select only one customer to edit.");
            }
        }

        /// <summary>
        /// The show insurance method.
        /// </summary>
        /// <param name="viewModel">The view model being passed in.</param>
        private void ShowInsurance(InsuranceViewModel viewModel)
        {
            WorkspaceWindow window = new WorkspaceWindow();                 
            window.Title = viewModel.DisplayName;

            viewModel.CloseAction = b => window.DialogResult = b;

            InsuranceView view = new InsuranceView();
            view.DataContext = viewModel;

            window.Content = view;
            window.ShowDialog();            
        }

        /// <summary>
        /// The on insurnace property removed method.
        /// </summary>
        /// <param name="sender">The sender being passed in.</param>
        /// <param name="e">The event being passed in.</param>
        private void OnInsuranceRemoved(object sender, InsuranceEventArgs e)
        {
            InsuranceViewModel viewModel = this.AllInsurances.SingleOrDefault(vm => vm.IsSelected);
            if (viewModel != null)
            {
                if (viewModel.Insurance == e.Insurance)
                {
                    this.AllInsurances.Remove(viewModel);
                }
            }
        }

        /// <summary>
        /// The delete insurance execute method.
        /// </summary>
        private void DeleteInsuranceExecute()
        {
            InsuranceViewModel viewModel = this.AllInsurances.SingleOrDefault(vm => vm.IsSelected);
            if (viewModel != null)
            {
                if (MessageBox.Show("Do you really want to delete the insurance?", "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    this.repository.RemoveInsurance(viewModel.Insurance);
                    this.repository.SaveToDatabase();
                }
            }
            else
            {
                MessageBox.Show("Please select only one patient.");
            }
        }
    }
}
