﻿using MedProEntryEngine;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;

namespace MedroInformationSystem
{
    /// <summary>
    /// The main window view model class.
    /// </summary>
    public class MainWindowViewModel : ViewModel
    {
        private ObservableCollection<ViewModel> viewModels;

        public MainWindowViewModel()
    : base("Medpro - Patient Management System")
        {

        }

        public ObservableCollection<ViewModel> ViewModels
        {
            get
            {
                if (this.viewModels == null)
                {
                    this.viewModels = new ObservableCollection<ViewModel>();
                }

                return this.viewModels;
            }
        }

        /// <summary>
        /// This method creates the views for the user.
        /// </summary>
        public void CreateViews()
        {
        }

        public void CreateNewPatient()
        {
            Patient patient = new Patient { FirstName = "Ginger", LastName = "Snaps", DateOfBirth = DateTime.Parse("06/06/2006") };
            PatientViewModel view = new PatientViewModel(patient);
            this.viewModels.Add(view);
            ActivateViewModel(view);
        }

        public void CreateNewInsurance()
        {
            Insurance insurance = new Insurance { CompanyName = "Quartz", InsuranceType = "Family", StartDate = DateTime.Parse("10/01/2013") };
            InsuranceViewModel view = new InsuranceViewModel(insurance);
            this.viewModels.Add(view);
            ActivateViewModel(view);
        }
        private void ActivateViewModel(ViewModel viewModel)
        {
            ICollectionView collectionView = CollectionViewSource.GetDefaultView(this.viewModels);

            if (collectionView != null)
            {
                collectionView.MoveCurrentTo(viewModel);
            }
        }
    }
}
