﻿using System;
using MedProDataAccess;
using MedProEntryEngine;

namespace MedroInformationSystem
{
    /// <summary>
    /// The note line view model class.
    /// </summary>
    public class NoteLineViewModel : WorkspaceViewModel
    {
        /// <summary>
        /// The note line field..
        /// </summary>
        private NoteLine line;

        /// <summary>
        /// The repository field
        /// </summary>
        private Repository repository;

        /// <summary>
        /// The is selected field.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="NoteLineViewModel"/> class.
        /// </summary>
        /// <param name="line">The line being passed in.</param>
        /// <param name="repository">The repository being passed in.</param>
        public NoteLineViewModel(NoteLine line, Repository repository)
            : base ("Patient Visit")
        {
            this.line = line;
            this.repository = repository;
        }

        /// <summary>
        /// Gets the value of the line property.
        /// </summary>
        public NoteLine Line
        {
            get
            {
                return this.line;
            }
        }
        /// <summary>
        /// Gets or sets the value indicating whether is selected is true or false.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
            }
        }

        /// <summary>
        /// Gets or sets the patient note property.
        /// </summary>
        public string PatientNote
        {
            get
            {
                return this.line.PatientNote;
            }
            set
            {
                this.line.PatientNote = value;
                this.OnPropertyChanged("PatientNote");
            }
        }

        /// <summary>
        /// Gets or sets the times stamp property.
        /// </summary>
        public DateTime Timestamp
        {
            get
            {
                return this.line.Timestamp;
            }

            set
            {
                this.line.Timestamp = DateTime.Now;
            }
        }

        /// <summary>
        /// A method that creates commands.
        /// </summary>
        protected override void CreateCommands()
        {
            this.Commands.Add(new CommandViewModel("OK", new DelegateCommand(p => this.OkExecute())));
            this.Commands.Add(new CommandViewModel("Cancel", new DelegateCommand(p => this.CancelExecute())));
        }

        /// <summary>
        /// This is the ok execute method.
        /// </summary>
        private void OkExecute()
        {
            this.Save();
            this.CloseAction(true);
        }

        /// <summary>
        /// This is the cancel execute method.
        /// </summary>
        private void CancelExecute()
        {
            this.CloseAction(false);
        }

        /// <summary>
        /// The 
        /// </summary>
        private void Save() 
        {
            this.repository.AddNote(this.line.Note);
            this.repository.AddLine(this.line);
            this.repository.SaveToDatabase();
        }   
    }
}
