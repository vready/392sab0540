﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System;
using System.Windows.Data;
using System.Windows.Input;
using MedProDataAccess;
using MedProEntryEngine;

namespace MedroInformationSystem
{
    /// <summary>
    /// The multi patient view model.
    /// </summary>
    public class MultiPatientViewModel : WorkspaceViewModel
    {
        /// <summary>
        /// The view model's repository class.
        /// </summary>
        private Repository repository;

        /// <summary>
        /// The product view source field.
        /// </summary>
        private CollectionViewSource patientViewSource;

        /// <summary>
        /// The sort column field.
        /// </summary>
        private string sortColumnName;

        /// <summary>
        /// The sort direction field.
        /// </summary>
        private ListSortDirection sortDirection;

        /// <summary>
        /// The display patients field.
        /// </summary>
        private ObservableCollection<PatientViewModel> displayPatients;

        /// <summary>
        /// The all patients sorted field.
        /// </summary>
        private List<PatientViewModel> allPatientsSorted;

        /// <summary>
        /// Initializes a new instance of the <see cref="MultiPatientViewModel"/> class.
        /// </summary>
        /// <param name="repository">The repository being passed in.</param>
        public MultiPatientViewModel(Repository repository)
            : base("All patients")
        {
            this.repository = repository;

            this.repository.PatientAdded += this.OnPatientAdded;
            this.repository.PatientRemoved += this.OnPatientRemoved;


            List<PatientViewModel> patients =
                (from p in this.repository.GetPatients()
                 select new PatientViewModel(p, this.repository)).ToList();

            this.allPatientsSorted = new List<PatientViewModel>();
            this.allPatientsSorted = patients;

            AddPropertyChangedEvent(patients);

            this.AllPatients = new ObservableCollection<PatientViewModel>(patients);

            this.displayPatients = new ObservableCollection<PatientViewModel>();
            this.Pager = new PagingViewModel(this.AllPatients.Count);
            this.Pager.CurrentPageChanged = this.OnPagedChanged;
            this.RebuildPageData();

            this.patientViewSource = new CollectionViewSource();
            this.patientViewSource.Source = this.DisplayedPatients;
            this.SortCommand = new DelegateCommand(this.Sort);
        }

        /// <summary>
        /// The add property changed event method.
        /// </summary>
        /// <param name="patients">The patients being passed in.</param>
        private void AddPropertyChangedEvent(List<PatientViewModel> patients)
        {
            patients.ForEach(pvm => pvm.PropertyChanged += this.OnPatientViewModelPropertyChanged);
        }

        /// <summary>
        /// Gets a value indicating whether is selected is true or false.
        /// </summary>
        public int NumberOfItemsSelected
        {
            get
            {
                return this.AllPatients.Count(pvm => pvm.IsSelected);
            }
        }

        /// <summary>
        /// Gets or sets the search text.
        /// </summary>
        public string SearchText { get; set; }

        /// <summary>
        /// Gets the filter commands.
        /// </summary>
        public ObservableCollection<CommandViewModel> FilterCommands { get; private set; }

        /// <summary>
        /// Gets the values of the insurance types.
        /// </summary>
        public IEnumerable<InsuranceType> InsuranceTypes
        {
            get
            {
                return Enum.GetValues(typeof(InsuranceType)) as IEnumerable<InsuranceType>;
            }
        }

        /// <summary>
        /// Gets or sets the displayed products.
        /// </summary>
        public ObservableCollection<PatientViewModel> DisplayedPatients
        {
            get
            {
                return this.displayPatients;
            }

            set
            {
                this.displayPatients = value;
                this.patientViewSource = new CollectionViewSource();
                this.patientViewSource.Source = this.DisplayedPatients;
            }
        }

        /// <summary>
        /// Gets the insurances.
        /// </summary>
        public IEnumerable<string> Insurances
        {
            get
            {
                var insurances = (from i in this.repository.GetInsurance()
                                  select i.CompanyName).ToList();
                return insurances.Distinct();
            }
        }

        /// <summary>
        /// Gets or sets the filtered type.
        /// </summary>
        public InsuranceType FilteredType { get; set; }

        /// <summary>
        /// Gets or sets the filtered plan.
        /// </summary>
        public string FilteredPlan { get; set; }

        /// <summary>
        /// Gets or sets the value of all patients.
        /// </summary>
        public ObservableCollection<PatientViewModel> AllPatients { get; set; }

        /// <summary>
        /// Gets the pager model.
        /// </summary>
        public PagingViewModel Pager { get; private set; }

        /// <summary>
        /// Gets the list of the sorted products.
        /// </summary>
        public ListCollectionView SortedPatients
        {
            get
            {
                return this.patientViewSource.View as ListCollectionView;
            }
        }

        /// <summary>
        /// Gets the sort command.
        /// </summary>
        public ICommand SortCommand { get; private set; }

        /// <summary>
        /// The create commands method.
        /// </summary>
        protected override void CreateCommands()
        {
            this.Commands.Add(new CommandViewModel("New...", new DelegateCommand(p => this.CreateNewPatientExecute())));
            this.Commands.Add(new CommandViewModel("Edit...", new DelegateCommand(p => this.EditPatientExecute(), p => this.NumberOfItemsSelected == 1)));
            this.Commands.Add(new CommandViewModel("Delete...", new DelegateCommand(p => this.DeletePatientExecute(), p => this.NumberOfItemsSelected == 1)));

            this.FilterCommands = new ObservableCollection<CommandViewModel>();

            this.FilterCommands.Add(new CommandViewModel("Filter by Type", new DelegateCommand(p => this.FilterType())));
            this.FilterCommands.Add(new CommandViewModel("Filter by Company", new DelegateCommand(p => this.FilterPlan())));
            this.FilterCommands.Add(new CommandViewModel("Search", new DelegateCommand(p => this.Search())));
            this.FilterCommands.Add(new CommandViewModel("Clear", new DelegateCommand(p => this.ClearFilters())));
        }

        /// <summary>
        /// The patient view model property change method.
        /// </summary>
        /// <param name="sender">The object being sent.</param>
        /// <param name="e">The event being sent.</param>
        private void OnPatientViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsSelected")
            {
                this.OnPropertyChanged("NumberOfItemsSelected");
            }
        }

        /// <summary>
        /// The on patient added method.
        /// </summary>
        /// <param name="sender">The object being sent.</param>
        /// <param name="e">The event being sent.</param>
        private void OnPatientAdded(object sender, PatientEventArgs e)
        {
            PatientViewModel viewModel = new PatientViewModel(e.Patient, this.repository);
            
            this.AllPatients.Add(viewModel);
            this.DisplayedPatients.Add(viewModel);
            this.OnPropertyChanged("DisplayedPatients");
            this.OnPropertyChanged("AllPatients");            
        }

        /// <summary>
        /// The on paged changed method.
        /// </summary>
        /// <param name="sender">The sender being passed in.</param>
        /// <param name="e">The argument being passed in.</param>
        private void OnPagedChanged(object sender, CurrentPageChangeEventArgs e)
        {
            this.RebuildPageData();
        }

        /// <summary>
        /// The rebuild page data method.
        /// </summary>
        public void RebuildPageData()
        {
            this.DisplayedPatients.Clear();

            var startingIndex = this.Pager.PageSize * (this.Pager.CurrentPage - 1);
            this.Pager.PatientCount = this.AllPatients.Count;

            List<PatientViewModel> displayPatients = this.allPatientsSorted.Skip(startingIndex).Take(this.Pager.PageSize).ToList();

            foreach (PatientViewModel p in displayPatients)
            {
                this.DisplayedPatients.Add(p);
            }

            this.AllPatients.Count(pvm => pvm.IsSelected = false);
        }

        /// <summary>
        /// The sort method.
        /// </summary>
        /// <param name="parameter">The parameter being passed in to be sorted.</param>
        public void Sort(object parameter)
        {
            string tempVariable = parameter as string;

            CollectionViewSource sortCollection = new CollectionViewSource();
            sortCollection.Source = this.AllPatients;

            if (this.sortColumnName == tempVariable)
            {
                if (this.sortDirection == ListSortDirection.Ascending)
                {
                    this.sortDirection = ListSortDirection.Descending;
                }
                else
                {
                    this.sortDirection = ListSortDirection.Ascending;
                }
            }
            else
            {
                this.sortColumnName = tempVariable;
                this.sortDirection = ListSortDirection.Ascending;
            }

            sortCollection.SortDescriptions.Clear();
            sortCollection.SortDescriptions.Add(new SortDescription(this.sortColumnName, this.sortDirection));

            var collection = sortCollection.View.Cast<PatientViewModel>().ToList();

            this.allPatientsSorted.Clear();

            foreach (PatientViewModel p in collection)
            {
                this.allPatientsSorted.Add(p);
            }

            this.RebuildPageData();
        }

        /// <summary>
        /// The create new patient method.
        /// </summary>
        private void CreateNewPatientExecute()
        {
            Patient patient = new Patient();
            PatientViewModel viewModel = new PatientViewModel(patient, this.repository);
            this.ShowPatient(viewModel);  
            
            /// This code removes a patient that has been added to the repository for the notes and insurance adds.
            /// If the user cancels the patient it removes the patient from the repository so that it does not
            /// Show up in the list of patients. 
            foreach(PatientViewModel p in this.AllPatients.ToList())
            {
                if (!p.Patient.IsNotNewPatient)
                {
                    this.AllPatients.Remove(p);
                    this.DisplayedPatients.Remove(p);
                    this.OnPropertyChanged("AllPatients");
                    this.OnPropertyChanged("DisplayedPatients");
                }
            }            
        }

        /// <summary>
        /// The method that shows the patient model that needs edited.
        /// </summary>
        /// <param name="viewModel">The patient view model being passed in.</param>
        private void ShowPatient(PatientViewModel viewModel)
        {
            WorkspaceWindow window = new WorkspaceWindow();
            window.Title = viewModel.DisplayName;
            window.Width = 1000;
            window.Height = 600;
            viewModel.CloseAction = b => window.DialogResult = b;

            PatientView view = new PatientView();            
            view.DataContext = viewModel;

            window.Content = view;
            window.ShowDialog();
        }

        /// <summary>
        /// The edit patient method.
        /// </summary>
        private void EditPatientExecute()
        {
            try
            {
                PatientViewModel viewModel = this.AllPatients.SingleOrDefault(vm => vm.IsSelected);

                if (viewModel != null)
                {
                    viewModel.FilteredAddressViewModel.AllAddresses = viewModel.FilteredAddresses;
                    this.OnPropertyChanged("AllAddresses");
                    viewModel.FilteredInsuranceViewModel.AllInsurances = viewModel.FilteredInsurance;
                    this.OnPropertyChanged("AllInsurances");
                    viewModel.FilteredNotesViewModel.AllNotes = viewModel.FilteredNotes;
                    this.OnPropertyChanged("AllNotes");
                    this.ShowPatient(viewModel);
                    this.repository.SaveToDatabase();
                }
                else
                {
                    MessageBox.Show("Please select only one patient to edit.");
                }
            }
            catch
            {
                MessageBox.Show("Please select only one patient to edit.");
            }
        }

        /// <summary>
        /// The method that detects a patient removed.
        /// </summary>
        /// <param name="sender">The sender being sent.</param>
        /// <param name="e">The event being passed in.</param>
        private void OnPatientRemoved(object sender, PatientEventArgs e)
        {
            PatientViewModel viewModel = this.AllPatients.SingleOrDefault(vm => vm.IsSelected);
            if (viewModel != null)
            {
                if (viewModel.Patient == e.Patient)
                {
                    this.AllPatients.Remove(viewModel);
                    this.DisplayedPatients.Remove(viewModel);
                    this.OnPropertyChanged("AllPatients");
                    this.OnPropertyChanged("DisplayedPatients");
                }
            }
        }

        /// <summary>
        /// The method that deletes a patient.
        /// </summary>
        private void DeletePatientExecute()

        {
            PatientViewModel viewModel = this.AllPatients.SingleOrDefault(vm => vm.IsSelected);
            if (viewModel != null)
            {
                if (MessageBox.Show("Do you really want to delete the patient?", "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    this.repository.RemovePatient(viewModel.Patient);
                    this.repository.SaveToDatabase();
                }
            }
            else
            {
                MessageBox.Show("Please select only one patient.");
            }
        }

        /// <summary>
        /// The method that filters by company name.
        /// </summary>
        private void FilterPlan()
        {
            this.AllPatients = this.RebuildAllPatients();

            var filteredPatients = (from p in this.AllPatients
                                    from i in p.FilteredInsurance
                                    where i.CompanyName.ToString() == this.FilteredPlan
                                    select new PatientViewModel(p.Patient, this.repository)).ToList();

            this.CheckFilteredPatients(filteredPatients);
        }

        /// <summary>
        /// The method that filters by insurance type.
        /// </summary>
        private void FilterType()
        {
            this.AllPatients = this.RebuildAllPatients();

            var filteredPatients = (from p in this.AllPatients
                                    from i in p.FilteredInsurance
                                    where i.InsuranceType == this.FilteredType
                                    select new PatientViewModel(p.Patient, this.repository)).ToList();

            this.CheckFilteredPatients(filteredPatients);
        }

        /// <summary>
        /// The method that clears all search items.
        /// </summary>
        private void ClearFilters()
        {
            this.FilteredType = InsuranceType.HMO;
            this.OnPropertyChanged("FilteredType");

            if (this.FilteredPlan != null)
            {
                this.FilteredPlan = this.Insurances.FirstOrDefault();
                this.OnPropertyChanged("FilteredPlan");
            }

            this.SearchText = string.Empty;
            this.OnPropertyChanged("SearchText");

            this.AllPatients = this.RebuildAllPatients();
            this.allPatientsSorted = new List<PatientViewModel>(this.AllPatients);

            this.OnPropertyChanged("AllPatients");

            this.RebuildPageData();
        }

        /// <summary>
        /// The method that searches for something.
        /// </summary>
        private void Search()
        {
            try
            {
                string searchText = this.SearchText.ToString().ToLower();

                if (!string.IsNullOrWhiteSpace(searchText))
                {
                    var filteredPatient =
                        (from p in this.repository.GetPatients()
                         where p.FirstName.ToLower().Contains(searchText) || p.LastName.ToLower().Contains(searchText) || (p.MiddleName != null && p.MiddleName.ToLower().Contains(searchText))
                         || (p.Email != null && p.Email.ToLower().Contains(searchText))

                         where p.FirstName.ToLower().Contains(searchText) || p.LastName.ToLower().Contains(searchText) || (p.MiddleName != null && p.MiddleName.ToLower().Contains(searchText)) || (p.Email != null && p.Email.ToLower().Contains(searchText)) 

                         select new PatientViewModel(p, this.repository)).ToList();
                    AddPropertyChangedEvent(filteredPatient);
                    this.AllPatients = new ObservableCollection<PatientViewModel>(filteredPatient);
                    this.allPatientsSorted = new List<PatientViewModel>(filteredPatient);
                    this.OnPropertyChanged("SortedPatients");
                    this.RebuildPageData();
                }
            }
            catch
            {
                MessageBox.Show("Please enter value into the search box before searching.");
            }
        }

        /// <summary>
        /// The method that returns all the patients back into the all patients property.
        /// </summary>
        /// <returns>An observable collection of all patients.</returns>
        private ObservableCollection<PatientViewModel> RebuildAllPatients()
        {
            List<PatientViewModel> patients =
                (from p in this.repository.GetPatients()
                 select new PatientViewModel(p, this.repository)).ToList();

            this.AddPropertyChangedEvent(patients);

            return new ObservableCollection<PatientViewModel>(patients);
        }

        /// <summary>
        /// Method that checks and applies the filtered patients. 
        /// </summary>
        /// <param name="filteredPatients"></param>
        private void CheckFilteredPatients(List<PatientViewModel> filteredPatients)
        {
            if (filteredPatients.Count != 0)
            {
                AddPropertyChangedEvent(filteredPatients);

                this.AllPatients = new ObservableCollection<PatientViewModel>(filteredPatients);
                this.allPatientsSorted = new List<PatientViewModel>(this.AllPatients);
                this.OnPropertyChanged("SortedPatients");
                this.RebuildPageData();
            }
            else
            {
                MessageBox.Show("No patient is found with " + this.FilteredType + ".");
            }
        }
    }
}
