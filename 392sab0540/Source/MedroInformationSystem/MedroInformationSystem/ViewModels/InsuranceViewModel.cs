﻿using System;
using System.Windows.Input;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using MedProDataAccess;
using MedProEntryEngine;

namespace MedroInformationSystem
{
    /// <summary>
    /// The insurance view model class.
    /// </summary>
    public class InsuranceViewModel : WorkspaceViewModel
    {
        /// <summary>
        /// The insurance field.
        /// </summary>
        private Insurance insurance;

        /// <summary>
        /// The insurance view model repository field.
        /// </summary>
        private Repository repository;

        /// <summary>
        /// The save command field.
        /// </summary>
        private ICommand saveCommand;

        /// <summary>
        /// The is selected field.
        /// </summary>
        private bool isSelected;

        private MultiAddressViewModel filteredAddressViewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="InsuranceViewModel"/> class.
        /// </summary>
        /// <param name="insurance">The insurance being passed in.</param>
        /// <param name="repository">The repository being passed in.</param>
        public InsuranceViewModel(Insurance insurance, Repository repository)
            : base("Insurance")
        {
            this.insurance = insurance;
            this.repository = repository;
            this.filteredAddressViewModel = new MultiAddressViewModel(this.repository, this.insurance);
            this.filteredAddressViewModel.AllAddresses = this.FilteredAddresses;
        }

        /// <summary>
        /// Gets the insurance for the view model.
        /// </summary>
        public Insurance Insurance
        {
            get
            {
                return this.insurance;
            }
        }

        /// <summary>
        /// Gets the filtered addresses for the view model.
        /// </summary>
        public MultiAddressViewModel FilteredAddressViewModel
        {
            get
            {
                return this.filteredAddressViewModel;
            }
        }

        /// <summary>
        /// Gets the filtered address for the view model.
        /// </summary>
        public ObservableCollection<AddressViewModel> FilteredAddresses
        {
            get
            {
                var addresses =
                    (from a in this.insurance.Addresses
                     select new AddressViewModel(a, this.repository)).ToList();

                this.FilteredAddressViewModel.AddPropertyChangedEvent(addresses);
                return new ObservableCollection<AddressViewModel>(addresses);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is selected is true or false.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
            }
        }

        /// <summary>
        /// Gets or sets the insurance company name.
        /// </summary>
        public string CompanyName
        {
            get
            {
                return this.insurance.CompanyName;
            }

            set
            {
                this.insurance.CompanyName = value;
                this.OnPropertyChanged("CompanyName");
            }
        }

        /// <summary>
        /// Gets or sets the insurance type.
        /// </summary>
        public InsuranceType InsuranceType
        {
            get
            {
                return this.insurance.InsuranceType;
            }

            set
            {
                this.insurance.InsuranceType = value;
                this.OnPropertyChanged("InsuranceType");
            }
        }

        /// <summary>
        /// Gets the list of insurance types.
        /// </summary>
        public IEnumerable<InsuranceType> Types
        {
            get
            {
                return Enum.GetValues(typeof(InsuranceType)) as IEnumerable<InsuranceType>;
            }
        }

        /// <summary>
        /// Gets or sets the insurance start date..
        /// </summary>
        public DateTime? StartDate
        {
            get
            {
                return this.insurance.StartDate;
            }

            set
            {
                this.insurance.StartDate = value;
                this.OnPropertyChanged("StartDate");
            }
        }

        /// <summary>
        /// Gets or sets the insurance end date.
        /// </summary>
        public DateTime? EndDate
        {
            get
            {
                if (this.insurance.EndDate == DateTime.MinValue)
                {
                    DateTime date = DateTime.MaxValue;
                    return date;
                }
                else
                {
                    return this.insurance.EndDate;
                }                
            }

            set
            {
                this.insurance.EndDate = value;
                this.OnPropertyChanged("EndDate");
            }
        }

        /// <summary>
        /// Gets the patient for the insurance.
        /// </summary>
        public Patient Patient
        {
            get
            {
                return this.insurance.Patient;
            }
        }

        /// <summary>
        /// Gets the current address for the insurance.
        /// </summary>
        public string CurrentAddress
        {
            get
            {
                try
                {
                    var address =
                        (from a in this.insurance.Addresses
                         where a.EndDate == null
                         select a.ToString()).First();

                    //(from x in Items where x.Id == 123 select x).First();
                    return address;
                }
                catch
                {
                    return "No address on file";
                }
            }
        }
               
        /// <summary>
        /// Gets the save command property.
        /// </summary>
        public ICommand SaveCommand
        {
            get
            {
                return this.saveCommand = new DelegateCommand(p => this.Save());
            }
        }

        /// <summary>
        /// The save method.
        /// </summary>
        public void Save()
        {
            this.repository.AddInsurance(this.insurance);
            this.repository.SaveToDatabase();
        }

        /// <summary>
        /// A method that creates commands.
        /// </summary>
        protected override void CreateCommands()
        {
            this.Commands.Add(new CommandViewModel("OK", new DelegateCommand(p => this.OkExecute())));
            this.Commands.Add(new CommandViewModel("Cancel", new DelegateCommand(p => this.CancelExecute())));
        }

        /// <summary>
        /// This is the ok execute method.
        /// </summary>
        private void OkExecute()
        {
            this.CloseAction(true);
            this.Save();
        }

        /// <summary>
        /// This is the cancel execute method.
        /// </summary>
        private void CancelExecute()
        {
            this.CloseAction(false);
        }
    }
}
