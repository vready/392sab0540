﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using MedProDataAccess;
using MedProEntryEngine;

namespace MedroInformationSystem
{
    /// <summary>
    /// The multi notes view model class.
    /// </summary>
    public class MultiNotesViewModel : WorkspaceViewModel
    {
        /// <summary>
        /// The multi notes view model repository field.
        /// </summary>
        private Repository repository;

        /// <summary>
        /// The patient associated with the view model.
        /// </summary>
        private Patient patient;

        /// <summary>
        /// Initializes a new instance of the <see cref="MultiNotesViewModel"/> class.
        /// </summary>
        /// <param name="repository">The repository being passed in.</param>
        public MultiNotesViewModel(Repository repository, Patient patient)
            : base("All visits")
        {
            this.repository = repository;
            this.patient = patient;
            this.Commands.Clear();
            this.CreateCommands();
            this.repository.NoteAdded += this.OnNoteAdded;
            this.repository.NoteRemoved += this.OnNoteRemoved;

            List<NotesViewModel> notes =
                (from n in this.repository.GetNotes()
                 select new NotesViewModel(n, this.repository)).ToList();
            this.AddPropertyChangedEvent(notes);

            this.AllNotes = new ObservableCollection<NotesViewModel>(notes);
        }

        /// <summary>
        /// The add property changed event.
        /// </summary>
        /// <param name="notes">The notes being passed in.</param>
        public void AddPropertyChangedEvent(List<NotesViewModel> notes)
        {
            notes.ForEach(nvm => nvm.PropertyChanged += this.OnNoteViewModelPropertyChanged);
        }

        /// <summary>
        /// Gets a value indicating whether is selected is true or false.
        /// </summary>
        public int NumberOfItemsSelected
        {
            get
            {
                return this.AllNotes.Count(nvm => nvm.IsSelected);
            }
        }

        /// <summary>
        /// Gets or sets the all notes list property.
        /// </summary>
        public ObservableCollection<NotesViewModel> AllNotes { get; set; }

        /// <summary>
        /// The create commands method.
        /// </summary>
        protected override void CreateCommands()
        {
            if (this.patient != null)
            {
                this.Commands.Add(new CommandViewModel("New...", new DelegateCommand(p => this.CreateNewNoteExecute())));
                this.Commands.Add(new CommandViewModel("Edit...", new DelegateCommand(p => this.EditNoteExecute(), p => this.NumberOfItemsSelected == 1)));
                this.Commands.Add(new CommandViewModel("Delete...", new DelegateCommand(p => this.DeleteNoteExecute(), p => this.NumberOfItemsSelected == 1)));
            }
        }

        /// <summary>
        /// The note property changed event method.
        /// </summary>
        /// <param name="sender">The object being sent.</param>
        /// <param name="e">The event being sent.</param>
        private void OnNoteViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsSelected")
            {
                this.OnPropertyChanged("NumberOfItemsSelected");
            }
        }

        /// <summary>
        /// The note added method.
        /// </summary>
        /// <param name="sender">The object being sent.</param>
        /// <param name="e">The event being sent.</param>
        private void OnNoteAdded(object sender, NoteEventArgs e)
        {
            NotesViewModel viewModel = new NotesViewModel(e.Note, this.repository);
            if (this.patient == null || viewModel.Patient == e.Note.Patient)
            {
                this.AllNotes.Add(viewModel);
            }
        }

        /// <summary>
        /// The create new patient method.
        /// </summary>
        private void CreateNewNoteExecute()
        {
            if (this.patient != null)
            {
                this.repository.AddPatient(this.patient);
            }

            NotesViewModel viewModel = new NotesViewModel(new Note() {Patient = this.patient }, this.repository);
            this.ShowNote(viewModel);
        }

        /// <summary>
        /// The method that shows the patient model that needs edited.
        /// </summary>
        /// <param name="viewModel">The patient view model being passed in.</param>
        private void ShowNote(NotesViewModel viewModel)
        {
            WorkspaceWindow window = new WorkspaceWindow();
            window.Width = 400;
            window.Title = viewModel.DisplayName;

            viewModel.CloseAction = b => window.DialogResult = b;

            NotesView view = new NotesView();
            view.DataContext = viewModel;

            window.Content = view;
            window.ShowDialog();
        }

        /// <summary>
        /// The edit patient method.
        /// </summary>
        private void EditNoteExecute()
        {
            try
            {
                NotesViewModel viewModel = this.AllNotes.SingleOrDefault(vm => vm.IsSelected);

                if (viewModel != null)
                {
                    viewModel.FilteredLineViewModel.AllLines = viewModel.FilteredLines;
                    this.OnPropertyChanged("AllLines");
                    this.ShowNote(viewModel);
                    this.repository.SaveToDatabase();
                }
                else
                {
                    MessageBox.Show("Please select only one note to edit.");
                }
            }
            catch
            {
                MessageBox.Show("Please select only one note to edit.");
            }
        }

        /// <summary>
        /// The on note removed method.
        /// </summary>
        /// <param name="sender">The sender being passed in.</param>
        /// <param name="e">The event being passed in.</param>
        private void OnNoteRemoved(object sender, NoteEventArgs e)
        {
            NotesViewModel viewModel = this.AllNotes.SingleOrDefault(vm => vm.IsSelected);
            if (viewModel != null)
            {
                if (viewModel.Note == e.Note)
                {
                    this.AllNotes.Remove(viewModel);
                }
            }
        }

        /// <summary>
        /// The delete note execute method.
        /// </summary>
        private void DeleteNoteExecute()
        {
            NotesViewModel viewModel = this.AllNotes.SingleOrDefault(vm => vm.IsSelected);
            if (viewModel != null)
            {
                if (MessageBox.Show("Do you really want to delete the visit?", "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    this.repository.RemoveNote(viewModel.Note);
                    this.repository.SaveToDatabase();
                }
            }
            else
            {
                MessageBox.Show("Please select only one visit.");
            }
        }
    }
}
