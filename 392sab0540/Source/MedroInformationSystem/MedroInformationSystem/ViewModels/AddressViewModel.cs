﻿using System;
using System.Collections.Generic;
using MedProDataAccess;
using MedProEntryEngine;

namespace MedroInformationSystem
{
    /// <summary>
    /// The address view model class.
    /// </summary>
    public class AddressViewModel : WorkspaceViewModel
    {
        /// <summary>
        /// The view model repository field.
        /// </summary>
        private Repository repository;

        /// <summary>
        /// The view model address field.
        /// </summary>
        private Address address;

        /// <summary>
        /// The is selected field.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="AddressViewModel"/> class.
        /// </summary>
        /// <param name="address">The address being passed in.</param>
        /// <param name="repository">The repository being passed in.</param>
        public AddressViewModel(Address address, Repository repository)
            : base("New Address")
        {
            this.repository = repository;
            this.address = address;
        }

        /// <summary>
        /// The address view model save method.
        /// </summary>
        public void Save()
        {
            if (this.Insurance != null)
            {
                this.repository.AddInsurance(this.Insurance);
            }
            else
            {
                this.repository.AddPatient(this.Patient);
            }            

            this.repository.AddAddress(this.address);
            this.repository.SaveToDatabase();
        }

       /// <summary>
       /// Gets or sets the address line 1 property.
       /// </summary>
        public string AddressLine1
        {
            get
            {
                return this.address.AddressLine1;
            }

            set
            {
                this.address.AddressLine1 = value;
                this.OnPropertyChanged("AddressLine1");
            }
        }

        /// <summary>
        /// Gets or sets the address line 2 property.
        /// </summary>
        public string AddressLine2
        {
            get
            {
                return this.address.AddressLine2;
            }

            set
            {
                this.address.AddressLine2 = value;
                this.OnPropertyChanged("AddressLine2");
            }
        }

        /// <summary>
        /// Gets or sets the address city property.
        /// </summary>
        public string City
        {
            get
            {
                return this.address.City;
            }

            set
            {
                this.address.City = value;
                this.OnPropertyChanged("City");                
            }
        }

        /// <summary>
        /// Gets or sets the address state property.
        /// </summary>
        public State State
        {
            get
            {
                return this.address.State;
            }

            set
            {
                this.address.State = value;
                this.OnPropertyChanged("State");
            }
        }

        /// <summary>
        /// Gets or sets the address zipcode property.
        /// </summary>
        public string ZipCode
        {
            get
            {
                return this.address.ZipCode;
            }

            set
            {
                this.address.ZipCode = value;
                this.OnPropertyChanged("ZipCode");
            }
        }

        /// <summary>
        /// Gets or sets the address start date property.
        /// </summary>
        public DateTime? StartDate
        {
            get
            {
                return this.address.StartDate;
            }

            set
            {
                this.address.StartDate = value;
                this.OnPropertyChanged("StartDate");
            }
        }

        /// <summary>
        /// Gets or sets the insurance end date.
        /// </summary>
        public DateTime? EndDate
        {
            get
            {

                    return this.address.EndDate;                
            }

            set
            {
                this.address.EndDate = value;
                this.OnPropertyChanged("EndDate");
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the is selected is true or false.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
            }
        }

        /// <summary>
        /// Gets the list of states.
        /// </summary>
        public IEnumerable<State> States
        {
            get
            {
                return Enum.GetValues(typeof(State)) as IEnumerable<State>;
            }
        }

        /// <summary>
        /// Gets the patient. 
        /// </summary>
        public Patient Patient
        {
            get
            {
                return this.address.Patient;
            }
        }

        /// <summary>
        /// Gets the insurance.
        /// </summary>
        public Insurance Insurance
        {
            get
            {
                return this.address.Insurance;
            }
        }

        /// <summary>
        /// Gets the address.
        /// </summary>
        public Address Address
        {
            get
            {
                return this.address;
            }
        }

        /// <summary>
        /// A method that creates commands.
        /// </summary>
        protected override void CreateCommands()
        {
            this.Commands.Add(new CommandViewModel("OK", new DelegateCommand(p => this.OkExecute())));
            this.Commands.Add(new CommandViewModel("Cancel", new DelegateCommand(p => this.CancelExecute())));
        }

        /// <summary>
        /// This is the ok execute method.
        /// </summary>
        private void OkExecute()
        {
            this.Save();
            this.CloseAction(true);
        }

        /// <summary>
        /// This is the cancel execute method.
        /// </summary>
        private void CancelExecute()
        {
            this.CloseAction(false);
        }
    }
}
