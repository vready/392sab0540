﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using MedProDataAccess;
using MedProEntryEngine;

namespace MedroInformationSystem
{
    /// <summary>
    /// The multie note line view model class.
    /// </summary>
    public class MultiNoteLineViewModel : WorkspaceViewModel
    {
        /// <summary>
        /// The repository for the database.
        /// </summary>
        private Repository repository;

        /// <summary>
        /// The note associated with the model.
        /// </summary>
        private Note note;

        /// <summary>
        /// Initializes a new instance of the <see cref="MultiNoteLineViewModel"/> class.
        /// </summary>
        /// <param name="repository">The repository being passed in.</param>
        /// <param name="note">The note being passed in.</param>
        public MultiNoteLineViewModel(Repository repository, Note note)
            :base ("All Visits")
        {
            this.repository = repository;
            this.note = note;
            this.repository.LineAdded += this.OnNoteLineAdded;
            this.repository.LineRemoved += this.OnNoteLineRemoved;
        }

        /// <summary>
        /// Gets or sets the all lines property.
        /// </summary>
        public ObservableCollection<NoteLineViewModel> AllLines { get; set; }

        /// <summary>
        /// Gets the number of selected items.
        /// </summary>
        public int NumberOfItemsSelected
        {
            get
            {
                return this.AllLines.Count(nvm => nvm.IsSelected);
            }
        }

        /// <summary>
        /// The add property changed event method.
        /// </summary>
        /// <param name="lines">The lines needing the event added to.</param>
        public void AddPropertyChangedEvent(List<NoteLineViewModel> lines)
        {
            lines.ForEach(nvm => nvm.PropertyChanged += this.OnNoteLineViewModelPropertyChanged);
        }

        /// <summary>
        /// The create commands method.
        /// </summary>
        protected override void CreateCommands()
        {
            this.Commands.Add(new CommandViewModel("New...", new DelegateCommand(p => this.CreateNewNoteLineExecute())));
            this.Commands.Add(new CommandViewModel("Edit...", new DelegateCommand(p => this.EditNoteLineExecute(), p => this.NumberOfItemsSelected == 1)));
            this.Commands.Add(new CommandViewModel("Delete...", new DelegateCommand(p => this.DeleteNoteLineExecute(), p => this.NumberOfItemsSelected == 1)));
        }

        /// <summary>
        /// The on note line added method.
        /// </summary>
        /// <param name="sender">The sender being passed in.</param>
        /// <param name="e">The event being passed in.</param>
        private void OnNoteLineAdded(object sender, NoteLineEventArgs e)
        {
            NoteLineViewModel viewModel = new NoteLineViewModel(e.Line, this.repository);
            viewModel.PropertyChanged += this.OnNoteLineViewModelPropertyChanged;
            this.AllLines.Add(viewModel);
        }

        /// <summary>
        /// The on note line view model property changed method.
        /// </summary>
        /// <param name="sender">The sender being passed in.</param>
        /// <param name="e">The event being passed in.</param>
        private void OnNoteLineViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsSelected")
            {
                this.OnPropertyChanged("NumberOfItemsSelected");
            }
        }

        /// <summary>
        /// The create new note line execute method.
        /// </summary>
        private void CreateNewNoteLineExecute()
        {
            NoteLine noteLine = new NoteLine { Note = this.note, Timestamp = DateTime.Now };

            NoteLineViewModel viewModel = new NoteLineViewModel(noteLine, this.repository);

            this.ShowNoteLine(viewModel);
        }

        /// <summary>
        /// The edit note line execute method.
        /// </summary>
        private void EditNoteLineExecute()
        {
            try
            {
                NoteLineViewModel viewModel = this.AllLines.SingleOrDefault(vm => vm.IsSelected);

                if (viewModel != null)
                {
                    this.ShowNoteLine(viewModel);
                    this.repository.SaveToDatabase();
                }
                else
                {
                    MessageBox.Show("Please select only one note line to edit.");
                }
            }
            catch
            {
                MessageBox.Show("Please select only one note line to edit.");
            }
        }

        /// <summary>
        /// The show note line method.
        /// </summary>
        /// <param name="viewModel">The note line view model being passed in.</param>
        private void ShowNoteLine(NoteLineViewModel viewModel)
        {
            WorkspaceWindow window = new WorkspaceWindow();
            window.Width = 400;
            viewModel.CloseAction = b => window.DialogResult = b;

            NoteLineView view = new NoteLineView();
            view.DataContext = viewModel;

            window.Content = view;
            window.ShowDialog();
        }

        /// <summary>
        /// The on note line removed method.
        /// </summary>
        /// <param name="sender">The sender being passed in.</param>
        /// <param name="e">The event being passed in.</param>
        private void OnNoteLineRemoved(object sender, NoteLineEventArgs e)
        {
            NoteLineViewModel viewModel = this.AllLines.SingleOrDefault(vm => vm.IsSelected);
            if (viewModel != null)
            {
                if (viewModel.Line == e.Line)
                {
                    this.AllLines.Remove(viewModel);
                }
            }
        }

        /// <summary>
        /// The delete note line execute method.
        /// </summary>
        private void DeleteNoteLineExecute()
        {
            NoteLineViewModel viewModel = this.AllLines.SingleOrDefault(vm => vm.IsSelected);
            if (viewModel != null)
            {
                if (MessageBox.Show("Do you really want to delete the note line?", "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    this.repository.RemoveLine(viewModel.Line);
                    this.repository.SaveToDatabase();
                }
            }
            else
            {
                MessageBox.Show("Please select only one order line.");
            }
        }
    }
}
