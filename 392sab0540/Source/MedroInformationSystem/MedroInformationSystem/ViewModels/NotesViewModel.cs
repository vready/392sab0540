﻿using System.Windows.Input;
using MedProDataAccess;
using MedProEntryEngine;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace MedroInformationSystem
{
    /// <summary>
    /// The notes view model class.
    /// </summary>
    public class NotesViewModel : WorkspaceViewModel
    {
        /// <summary>
        /// The notes view model note field.
        /// </summary>
        private Note note;

        /// <summary>
        /// The notes view model repository field.
        /// </summary>
        private Repository repository;

        /// <summary>
        /// The save command field.
        /// </summary>
        private ICommand saveCommand;

        /// <summary>
        /// The is selected field.
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// The filtered line view model field.
        /// </summary>
        private MultiNoteLineViewModel filteredLineViewModel;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotesViewModel"/> class.
        /// </summary>
        /// <param name="note">The note being passed in.</param>
        /// <param name="repository">The repository being passed in.</param>
        public NotesViewModel(Note note, Repository repository)
            : base("Visit")
        {
            this.repository = repository;
            this.note = note;
            this.filteredLineViewModel = new MultiNoteLineViewModel(this.repository, this.note);
            this.filteredLineViewModel.AllLines = this.FilteredLines;
        }

        public Note Note
        {
            get
            {
                return this.note;
            }
        }

        /// <summary>
        /// Gets the filtered view model.
        /// </summary>
        public MultiNoteLineViewModel FilteredLineViewModel
        {
            get
            {
                return this.filteredLineViewModel;
            }
        }

        /// <summary>
        /// Gets the list of lines for the note.
        /// </summary>
        public ObservableCollection<NoteLineViewModel> FilteredLines
        {
            get
            {
                var lines =
                    (from l in this.note.Lines
                     select new NoteLineViewModel(l, this.repository)).ToList();

                this.FilteredLineViewModel.AddPropertyChangedEvent(lines);
                return new ObservableCollection<NoteLineViewModel>(lines);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether is selected is true or false.
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return this.isSelected;
            }

            set
            {
                this.isSelected = value;
                this.OnPropertyChanged("IsSelected");
            }
        }
            

        /// <summary>
        /// Gets or sets the visit date field.
        /// </summary>
        public DateTime? VisitDate
        {
            get
            {
                if (this.note.VisitDate == null)
                {
                    return DateTime.Today;
                }
                else
                {
                    return this.note.VisitDate;
                }
                
            }
            set
            {

                this.note.VisitDate = value;
                this.OnPropertyChanged("VisitDate");
            }
        }
        
        /// <summary>
        /// Gets or sets the patient property.
        /// </summary>
        public Patient Patient
        {
            get
            {
                return this.note.Patient;
            }
        }

        /// <summary>
        /// Gets the save command property.
        /// </summary>
        public ICommand SaveCommand
        {
            get
            {
                return this.saveCommand = new DelegateCommand(p => this.Save());
            }
        }

        /// <summary>
        /// The save method.
        /// </summary>
        public void Save()
        {
            this.repository.AddNote(this.note);
            this.repository.SaveToDatabase();
        }

        /// <summary>
        /// A method that creates commands.
        /// </summary>
        protected override void CreateCommands()
        {
            this.Commands.Add(new CommandViewModel("OK", new DelegateCommand(p => this.OkExecute())));
            this.Commands.Add(new CommandViewModel("Cancel", new DelegateCommand(p => this.CancelExecute())));
        }

        /// <summary>
        /// This is the ok execute method.
        /// </summary>
        private void OkExecute()
        {
            this.Save();
            this.CloseAction(true);

        }

        /// <summary>
        /// This is the cancel execute method.
        /// </summary>
        private void CancelExecute()
        {
            this.CloseAction(false);
        }
    }
}
