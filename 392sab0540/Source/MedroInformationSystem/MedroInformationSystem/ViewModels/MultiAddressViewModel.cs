﻿using MedProDataAccess;
using MedProEntryEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;

namespace MedroInformationSystem
{
    public class MultiAddressViewModel : WorkspaceViewModel
    {
        /// <summary>
        /// The multi address view model repository field.
        /// </summary>
        private Repository repository;

        /// <summary>
        /// The patient associated with the view model.
        /// </summary>
        private Patient patient;

        private Insurance insurance;

        public MultiAddressViewModel(Repository repository, Patient patient)
            : base("All Addresses")
        {
            this.patient = patient;
            this.repository = repository;
            this.Commands.Clear();
            this.CreateCommands();
            this.repository.AddressAdded += this.OnAddressAdded;
            this.repository.AddressRemoved += this.OnAddressRemoved;

            PopulateAllAdresses();
        }


        public MultiAddressViewModel(Repository repository, Insurance insurance)
            : base("All Addresses")
        {
            this.insurance = insurance;
            this.repository = repository;
            this.Commands.Clear();
            this.CreateCommands();
            this.repository.AddressAdded += this.OnAddressAdded;
            this.repository.AddressRemoved += this.OnAddressRemoved;

            PopulateAllAdresses();
        }

        private void PopulateAllAdresses()
        {
            List<AddressViewModel> addresses =
                (from a in this.repository.GetAddresses()
                 select new AddressViewModel(a, this.repository)).ToList();
            this.AddPropertyChangedEvent(addresses);

            this.AllAddresses = new ObservableCollection<AddressViewModel>(addresses);
        }

        public void AddPropertyChangedEvent(List<AddressViewModel> addresses)
        {
            addresses.ForEach(ivm => ivm.PropertyChanged += this.OnAddressViewModelPropertyChanged);
        }

        public int NumberOfItemsSelected
        {
            get
            {
                return this.AllAddresses.Count(nvm => nvm.IsSelected);
            }
        }

        public ObservableCollection<AddressViewModel> AllAddresses { get; set; }

        /// <summary>
        /// The create commands method.
        /// </summary>
        protected override void CreateCommands()
        {
            if (this.patient != null || this.insurance != null)
            {
                this.Commands.Add(new CommandViewModel("New...", new DelegateCommand(p => this.CreateNewAddressExecute())));
                this.Commands.Add(new CommandViewModel("Edit...", new DelegateCommand(p => this.EditAddressExecute(), p => this.NumberOfItemsSelected == 1)));
                this.Commands.Add(new CommandViewModel("Delete...", new DelegateCommand(p => this.DeleteAddressExecute(), p => this.NumberOfItemsSelected == 1)));
            }
        }

        private void OnAddressViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "IsSelected")
            {
                this.OnPropertyChanged("NumberOfItemsSelected");
            }
        }

        private void OnAddressAdded(object sender, AddressEventArgs e)
        {
            AddressViewModel viewModel = new AddressViewModel(e.Address, this.repository);
            if (this.patient == null || viewModel.Patient == e.Address.Patient || this.insurance == null || viewModel.Insurance == e.Address.Insurance)
            {
                viewModel.PropertyChanged += this.OnAddressViewModelPropertyChanged;
                this.AllAddresses.Add(viewModel);
                this.OnPropertyChanged("AllAddresses");
            }
        }

        private void CreateNewAddressExecute()
        {
            if(this.patient != null)
            {
                AddressViewModel viewModel = new AddressViewModel(new Address() { Patient = this.patient }, this.repository);
                this.ShowAddress(viewModel);
            }
            if(this.insurance != null)
            {
                AddressViewModel viewModel = new AddressViewModel(new Address() { Insurance = this.insurance }, this.repository);
                this.ShowAddress(viewModel);
            }
            
        }

        private void EditAddressExecute()
        {
            try
            {
                AddressViewModel viewModel = this.AllAddresses.SingleOrDefault(vm => vm.IsSelected);

                if (viewModel != null)
                {
                    this.ShowAddress(viewModel);
                    this.repository.SaveToDatabase();
                }
                else
                {
                    MessageBox.Show("Please select only one address to edit.");
                }
            }
            catch
            {
                MessageBox.Show("Please select only one address to edit.");
            }
        }

        private void ShowAddress(AddressViewModel viewModel)
        {
            WorkspaceWindow window = new WorkspaceWindow();
            window.Width = 1000;
            window.Title = viewModel.DisplayName;

            viewModel.CloseAction = b => window.DialogResult = b;

            AddressView view = new AddressView();
            view.DataContext = viewModel;

            window.Content = view;
            window.ShowDialog();
        }

        private void OnAddressRemoved(object sender, AddressEventArgs e)
        {
            AddressViewModel viewModel = this.AllAddresses.SingleOrDefault(vm => vm.IsSelected);
            if (viewModel != null)
            {
                if (viewModel.Address == e.Address)
                {
                    this.AllAddresses.Remove(viewModel);
                }
            }
        }

        private void DeleteAddressExecute()

        {
            AddressViewModel viewModel = this.AllAddresses.SingleOrDefault(vm => vm.IsSelected);
            if (viewModel != null)
            {
                if (MessageBox.Show("Do you really want to delete the address?", "Confirmation", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    this.repository.RemoveAddress(viewModel.Address);
                    this.repository.SaveToDatabase();
                }
            }
            else
            {
                MessageBox.Show("Please select only one address.");
            }
        }
    }
}
