﻿namespace MedroInformationSystem
{
    /// <summary>
    /// The view model class.
    /// </summary>
    public abstract class ViewModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ViewModel"/> class.
        /// </summary>
        /// <param name="displayName">The display name of the view model.</param>
        public ViewModel(string displayName)
        {
            this.DisplayName = displayName;
        }

        /// <summary>
        /// Gets the property of Display name.
        /// </summary>
        public string DisplayName { get; private set; }
    }
}
